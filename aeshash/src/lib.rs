//! AESHash is a hash function based on AES-128.
//!
#![no_std]

use core::fmt;
use core::mem::MaybeUninit;

use aes::cipher::BlockEncrypt;
use digest::{
    block_buffer::Eager,
    core_api::{
        AlgorithmName, Block, BlockSizeUser, Buffer, BufferKindUser, CoreWrapper, FixedOutputCore,
        UpdateCore,
    },
    typenum::U16,
    HashMarker, Output, OutputSizeUser,
};

pub use digest::{self, Digest};

pub struct AESHashCore {
    aes_enc: aes::Aes128Enc,
    b: Block<Self>,
}

impl HashMarker for AESHashCore {}

impl OutputSizeUser for AESHashCore {
    type OutputSize = U16;
}

impl BlockSizeUser for AESHashCore {
    type BlockSize = U16;
}

impl BufferKindUser for AESHashCore {
    type BufferKind = Eager;
}

impl UpdateCore for AESHashCore {
    #[inline]
    fn update_blocks(&mut self, blocks: &[Block<Self>]) {
        let mut scratch = unsafe { MaybeUninit::<Block<Self>>::uninit().assume_init() };
        for block in blocks {
            self.aes_enc.encrypt_block_b2b(block, &mut scratch);
            for i in 0..16 {
                self.b[i] = self.b[i] ^ scratch[i];
            }
        }
    }
}

impl FixedOutputCore for AESHashCore {
    #[inline]
    fn finalize_fixed_core(&mut self, buffer: &mut Buffer<Self>, out: &mut Output<Self>) {
        let mut block = buffer.pad_with_zeros();
        self.aes_enc.encrypt_block(&mut block);
        for i in 0..16 {
            self.b[i] = self.b[i] ^ block[i];
        }
        out.copy_from_slice(self.b.as_slice());
    }
}

impl AlgorithmName for AESHashCore {
    fn write_alg_name(f: &mut core::fmt::Formatter<'_>) -> core::fmt::Result {
        write!(f, "AESHash")
    }
}

impl fmt::Debug for AESHashCore {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.write_str("AESHash { ... }")
    }
}

const AES_KEY: [u8; 16] = [
    44, 181, 209, 6, 96, 84, 97, 220, 124, 43, 123, 254, 15, 228, 80, 37,
];

impl Default for AESHashCore {
    fn default() -> Self {
        use aes::cipher::{generic_array::GenericArray, KeyInit};

        let key = GenericArray::from(AES_KEY);

        let aes_enc = aes::Aes128Enc::new(&key);
        Self {
            aes_enc,
            b: Block::<Self>::default(),
        }
    }
}

pub type AESHash = CoreWrapper<AESHashCore>;

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_hash() {
        let mut h = AESHash::default();
        h.update(b"hello world this is my message");
        let out = h.finalize();
        assert_eq!(
            out.as_slice(),
            &[83_u8, 255, 0, 162, 172, 226, 197, 132, 58, 195, 43, 1, 109, 123, 201, 10]
        );
    }

    #[test]
    fn test_zero() {
        let h = AESHash::default();
        let out = h.finalize();
        assert_eq!(
            out.as_slice(),
            &[244_u8, 215, 190, 30, 78, 0, 209, 151, 67, 173, 91, 171, 244, 102, 111, 64],
        );
    }
}

use core::{convert::TryInto, marker::PhantomData};

use serde::de::{self, DeserializeSeed, IntoDeserializer, Visitor};

use crate::{Error, Result};

use super::flavors::{Flavor, Slice};

pub struct Deserializer<'de, F: Flavor<'de>> {
    flavor: F,
    _plt: PhantomData<&'de ()>,
}

impl<'de, F> Deserializer<'de, F>
where
    F: Flavor<'de> + 'de,
{
    /// Obtain a Deserializer from a slice of bytes
    pub fn from_flavor(flavor: F) -> Self {
        Deserializer {
            flavor,
            _plt: PhantomData,
        }
    }

    /// Return the remaining (unused) bytes in the Deserializer along with any
    /// additional data provided by the [`Flavor`]
    pub fn finalize(self) -> Result<F::Remainder> {
        self.flavor.finalize()
    }
}

impl<'de> Deserializer<'de, Slice<'de>> {
    /// Obtain a Deserializer from a slice of bytes
    pub fn from_bytes(input: &'de [u8]) -> Self {
        Deserializer {
            flavor: Slice::new(input),
            _plt: PhantomData,
        }
    }
}

impl<'de, F: Flavor<'de>> Deserializer<'de, F> {}

impl<'de, 'a, F: Flavor<'de>> de::Deserializer<'de> for &'a mut Deserializer<'de, F> {
    type Error = Error;

    fn is_human_readable(&self) -> bool {
        false
    }

    fn deserialize_any<V>(self, _visitor: V) -> Result<V::Value>
    where
        V: de::Visitor<'de>,
    {
        // We won't ever support this
        Err(Error::WontImplement)
    }

    fn deserialize_bool<V>(self, visitor: V) -> Result<V::Value>
    where
        V: de::Visitor<'de>,
    {
        let v = match self.flavor.pop()? {
            0 => false,
            1 => true,
            _ => return Err(Error::DeserializeInvalidData),
        };
        visitor.visit_bool(v)
    }

    fn deserialize_i8<V>(self, visitor: V) -> Result<V::Value>
    where
        V: de::Visitor<'de>,
    {
        visitor.visit_i8(self.flavor.pop()? as i8)
    }

    fn deserialize_i16<V>(self, visitor: V) -> Result<V::Value>
    where
        V: de::Visitor<'de>,
    {
        let v = i16::from_le_bytes(
            self.flavor
                .try_take_n(2)?
                .try_into()
                .map_err(|_e| Error::DeserializeInvalidData)?,
        );
        visitor.visit_i16(v)
    }

    fn deserialize_i32<V>(self, visitor: V) -> Result<V::Value>
    where
        V: de::Visitor<'de>,
    {
        let v = i32::from_le_bytes(
            self.flavor
                .try_take_n(4)?
                .try_into()
                .map_err(|_e| Error::DeserializeInvalidData)?,
        );
        visitor.visit_i32(v)
    }

    fn deserialize_i64<V>(self, visitor: V) -> Result<V::Value>
    where
        V: de::Visitor<'de>,
    {
        let v = i64::from_le_bytes(
            self.flavor
                .try_take_n(8)?
                .try_into()
                .map_err(|_e| Error::DeserializeInvalidData)?,
        );
        visitor.visit_i64(v)
    }

    fn deserialize_u8<V>(self, visitor: V) -> Result<V::Value>
    where
        V: de::Visitor<'de>,
    {
        visitor.visit_u8(self.flavor.pop()? as u8)
    }

    fn deserialize_u16<V>(self, visitor: V) -> Result<V::Value>
    where
        V: de::Visitor<'de>,
    {
        let v = u16::from_le_bytes(
            self.flavor
                .try_take_n(2)?
                .try_into()
                .map_err(|_e| Error::DeserializeInvalidData)?,
        );
        visitor.visit_u16(v)
    }

    fn deserialize_u32<V>(self, visitor: V) -> Result<V::Value>
    where
        V: de::Visitor<'de>,
    {
        let v = u32::from_le_bytes(
            self.flavor
                .try_take_n(4)?
                .try_into()
                .map_err(|_e| Error::DeserializeInvalidData)?,
        );
        visitor.visit_u32(v)
    }

    fn deserialize_u64<V>(self, visitor: V) -> Result<V::Value>
    where
        V: de::Visitor<'de>,
    {
        let v = u64::from_le_bytes(
            self.flavor
                .try_take_n(8)?
                .try_into()
                .map_err(|_e| Error::DeserializeInvalidData)?,
        );
        visitor.visit_u64(v)
    }

    fn deserialize_f32<V>(self, visitor: V) -> Result<V::Value>
    where
        V: de::Visitor<'de>,
    {
        let v = f32::from_le_bytes(
            self.flavor
                .try_take_n(4)?
                .try_into()
                .map_err(|_e| Error::DeserializeInvalidData)?,
        );
        visitor.visit_f32(v)
    }

    fn deserialize_f64<V>(self, visitor: V) -> Result<V::Value>
    where
        V: de::Visitor<'de>,
    {
        let v = f64::from_le_bytes(
            self.flavor
                .try_take_n(8)?
                .try_into()
                .map_err(|_e| Error::DeserializeInvalidData)?,
        );
        visitor.visit_f64(v)
    }

    fn deserialize_char<V>(self, visitor: V) -> Result<V::Value>
    where
        V: de::Visitor<'de>,
    {
        let len = u32::from_le_bytes(
            self.flavor
                .try_take_n(4)?
                .try_into()
                .map_err(|_e| Error::DeserializeInvalidData)?,
        );
        let data: &'de [u8] = self.flavor.try_take_n(len as usize)?;
        let character = core::str::from_utf8(data)
            .map_err(|_e| Error::DeserializeInvalidData)?
            .chars()
            .next()
            .ok_or(Error::DeserializeInvalidData)?;
        visitor.visit_char(character)
    }

    fn deserialize_str<V>(self, visitor: V) -> Result<V::Value>
    where
        V: de::Visitor<'de>,
    {
        let len = u32::from_le_bytes(
            self.flavor
                .try_take_n(4)?
                .try_into()
                .map_err(|_e| Error::DeserializeInvalidData)?,
        );
        let data: &'de [u8] = self.flavor.try_take_n(len as usize)?;
        let str_utf = core::str::from_utf8(data).map_err(|_e| Error::DeserializeInvalidData)?;
        visitor.visit_borrowed_str(str_utf)
    }

    fn deserialize_string<V>(self, visitor: V) -> Result<V::Value>
    where
        V: de::Visitor<'de>,
    {
        self.deserialize_str(visitor)
    }

    fn deserialize_bytes<V>(self, visitor: V) -> Result<V::Value>
    where
        V: de::Visitor<'de>,
    {
        let len = u32::from_le_bytes(
            self.flavor
                .try_take_n(4)?
                .try_into()
                .map_err(|_e| Error::DeserializeInvalidData)?,
        );
        let data: &'de [u8] = self.flavor.try_take_n(len as usize)?;
        visitor.visit_borrowed_bytes(data)
    }

    fn deserialize_byte_buf<V>(self, visitor: V) -> Result<V::Value>
    where
        V: de::Visitor<'de>,
    {
        self.deserialize_bytes(visitor)
    }

    fn deserialize_option<V>(self, visitor: V) -> Result<V::Value>
    where
        V: de::Visitor<'de>,
    {
        match self.flavor.pop()? {
            0 => visitor.visit_none(),
            1 => visitor.visit_some(self),
            _ => Err(Error::DeserializeInvalidData),
        }
    }

    fn deserialize_unit<V>(self, visitor: V) -> Result<V::Value>
    where
        V: de::Visitor<'de>,
    {
        visitor.visit_unit()
    }

    fn deserialize_unit_struct<V>(self, _name: &'static str, visitor: V) -> Result<V::Value>
    where
        V: de::Visitor<'de>,
    {
        self.deserialize_unit(visitor)
    }

    fn deserialize_newtype_struct<V>(self, _name: &'static str, visitor: V) -> Result<V::Value>
    where
        V: de::Visitor<'de>,
    {
        visitor.visit_newtype_struct(self)
    }

    fn deserialize_seq<V>(self, visitor: V) -> Result<V::Value>
    where
        V: de::Visitor<'de>,
    {
        let len = u32::from_le_bytes(
            self.flavor
                .try_take_n(4)?
                .try_into()
                .map_err(|_e| Error::DeserializeInvalidData)?,
        ) as usize;
        visitor.visit_seq(SeqAccess {
            deserializer: self,
            len,
        })
    }

    fn deserialize_tuple<V>(self, len: usize, visitor: V) -> Result<V::Value>
    where
        V: de::Visitor<'de>,
    {
        visitor.visit_seq(SeqAccess {
            deserializer: self,
            len,
        })
    }

    fn deserialize_tuple_struct<V>(
        self,
        _name: &'static str,
        len: usize,
        visitor: V,
    ) -> Result<V::Value>
    where
        V: de::Visitor<'de>,
    {
        self.deserialize_tuple(len, visitor)
    }

    fn deserialize_map<V>(self, visitor: V) -> Result<V::Value>
    where
        V: de::Visitor<'de>,
    {
        let len = u32::from_le_bytes(
            self.flavor
                .try_take_n(4)?
                .try_into()
                .map_err(|_e| Error::DeserializeInvalidData)?,
        ) as usize;
        visitor.visit_map(MapAccess {
            deserializer: self,
            len,
        })
    }

    fn deserialize_struct<V>(
        self,
        _name: &'static str,
        fields: &'static [&'static str],
        visitor: V,
    ) -> Result<V::Value>
    where
        V: de::Visitor<'de>,
    {
        self.deserialize_tuple(fields.len(), visitor)
    }

    fn deserialize_enum<V>(
        self,
        _name: &'static str,
        _variants: &'static [&'static str],
        visitor: V,
    ) -> Result<V::Value>
    where
        V: de::Visitor<'de>,
    {
        visitor.visit_enum(self)
    }

    fn deserialize_identifier<V>(self, _visitor: V) -> Result<V::Value>
    where
        V: de::Visitor<'de>,
    {
        // Will not support
        Err(Error::WontImplement)
    }

    fn deserialize_ignored_any<V>(self, _visitor: V) -> Result<V::Value>
    where
        V: de::Visitor<'de>,
    {
        // Will not support
        Err(Error::WontImplement)
    }
}

struct SeqAccess<'a, 'b: 'a, F: Flavor<'b>> {
    deserializer: &'a mut Deserializer<'b, F>,
    len: usize,
}

impl<'a, 'b: 'a, F: Flavor<'b>> de::SeqAccess<'b> for SeqAccess<'a, 'b, F> {
    type Error = Error;

    #[inline]
    fn next_element_seed<V: DeserializeSeed<'b>>(&mut self, seed: V) -> Result<Option<V::Value>> {
        if self.len > 0 {
            self.len -= 1;
            Ok(Some(DeserializeSeed::deserialize(
                seed,
                &mut *self.deserializer,
            )?))
        } else {
            Ok(None)
        }
    }

    #[inline]
    fn size_hint(&self) -> Option<usize> {
        Some(self.len)
    }
}

struct MapAccess<'a, 'b: 'a, F: Flavor<'b>> {
    deserializer: &'a mut Deserializer<'b, F>,
    len: usize,
}

impl<'a, 'b: 'a, F: Flavor<'b>> de::MapAccess<'b> for MapAccess<'a, 'b, F> {
    type Error = Error;

    #[inline]
    fn next_key_seed<K: DeserializeSeed<'b>>(&mut self, seed: K) -> Result<Option<K::Value>> {
        if self.len > 0 {
            self.len -= 1;
            Ok(Some(DeserializeSeed::deserialize(
                seed,
                &mut *self.deserializer,
            )?))
        } else {
            Ok(None)
        }
    }

    #[inline]
    fn next_value_seed<V: DeserializeSeed<'b>>(&mut self, seed: V) -> Result<V::Value> {
        DeserializeSeed::deserialize(seed, &mut *self.deserializer)
    }

    #[inline]
    fn size_hint(&self) -> Option<usize> {
        Some(self.len)
    }
}

impl<'de, 'a, F: Flavor<'de>> de::EnumAccess<'de> for &'a mut Deserializer<'de, F> {
    type Error = Error;
    type Variant = Self;

    fn variant_seed<V: DeserializeSeed<'de>>(self, seed: V) -> Result<(V::Value, Self)> {
        let varint = self.flavor.pop()?;
        let v = DeserializeSeed::deserialize(seed, varint.into_deserializer())?;
        Ok((v, self))
    }
}

impl<'de, 'a, F: Flavor<'de>> de::VariantAccess<'de> for &'a mut Deserializer<'de, F> {
    type Error = Error;

    #[inline]
    fn unit_variant(self) -> Result<()> {
        Ok(())
    }

    #[inline]
    fn newtype_variant_seed<V: DeserializeSeed<'de>>(self, seed: V) -> Result<V::Value> {
        DeserializeSeed::deserialize(seed, self)
    }

    #[inline]
    fn tuple_variant<V: Visitor<'de>>(self, len: usize, visitor: V) -> Result<V::Value> {
        de::Deserializer::deserialize_tuple(self, len, visitor)
    }

    #[inline]
    fn struct_variant<V: Visitor<'de>>(
        self,
        fields: &'static [&'static str],
        visitor: V,
    ) -> Result<V::Value> {
        de::Deserializer::deserialize_tuple(self, fields.len(), visitor)
    }
}

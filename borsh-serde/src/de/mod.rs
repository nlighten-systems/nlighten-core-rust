use crate::Result;
use serde::Deserialize;

mod deserializer;
mod flavors;

pub use deserializer::Deserializer;

pub fn from_bytes<'a, T>(buf: &'a [u8]) -> Result<T>
where
    T: Deserialize<'a>,
{
    let mut deserializer = Deserializer::from_bytes(buf);
    let t = T::deserialize(&mut deserializer)?;
    Ok(t)
}

/// Deserialize a message of type `T` from a byte slice. The unused portion (if any)
/// of the byte slice is returned for further usage
pub fn take_from_bytes<'a, T>(s: &'a [u8]) -> Result<(T, &'a [u8])>
where
    T: Deserialize<'a>,
{
    let mut deserializer = Deserializer::from_bytes(s);
    let t = T::deserialize(&mut deserializer)?;
    Ok((t, deserializer.finalize()?))
}

#[cfg(test)]
mod test {

    use super::*;

    // #[test]
    // fn ser_u8() {
    //     let mut buf = [0_u8 ; 4];
    //     let buf = to_slice(&7_u8, &mut buf).unwrap();
    //     assert_eq!([7_u8], buf);
    // }

    #[test]
    fn de_u16() {
        let buf = [0x43_u8, 0x23_u8];
        let v: u16 = from_bytes(&buf).unwrap();
        assert_eq!(v, 0x2343);
    }

    #[test]
    fn test_de_unit_enum() {
        #[derive(Debug, PartialEq, Eq, serde_derive::Serialize, serde_derive::Deserialize)]
        enum MyEnum {
            One,
            Two,
            Three,
        }

        let v: MyEnum = from_bytes(&[0x1]).unwrap();
        assert_eq!(MyEnum::Two, v);
    }

    #[test]
    fn test_de_struct() {
        #[derive(Debug, serde_derive::Serialize, serde_derive::Deserialize, PartialEq, Eq)]
        struct MyStruct<'a> {
            data: &'a [u8],
            my_int: i32,
        }

        let buf = [
            0x0b_u8, 0x00, 0x00, 0x00, 0x68, 0x65, 0x6c, 0x6c, 0x6f, 0x20, 0x77, 0x6f, 0x72, 0x6c,
            0x64, 0x3a, 0x01, 0x00, 0x00,
        ];
        let v: MyStruct = from_bytes(&buf).unwrap();

        let v2 = MyStruct {
            data: b"hello world",
            my_int: 314,
        };

        assert_eq!(v, v2);

        // let mut buf = [0_u8 ; 128];
        // let buf = to_slice(&MyStruct {
        //     data: b"hello world",
        //     my_int: 314,
        // }, &mut buf).unwrap();

        // println!("{:02x?}", &buf[..]);
    }
}

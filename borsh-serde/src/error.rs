use core::fmt::Display;

use serde::{de, ser};

pub type Result<T> = core::result::Result<T, Error>;

#[derive(Clone, Debug, Eq, PartialEq)]
pub enum Error {
    NaNNotAllowed,
    VariantIndexTooBig,
    SerializeBufferFull,
    UnknownSequenceLength,
    DeserializeUnexpectedEnd,
    SerdeSerCustom,
    SerdeDeCustom,
    WontImplement,
    DeserializeInvalidData,
    CollectStrError,
    SerializeIO,
}

impl ser::Error for Error {
    fn custom<T: Display>(_msg: T) -> Self {
        Error::SerdeSerCustom
    }
}

impl de::Error for Error {
    fn custom<T: Display>(_msg: T) -> Self {
        Error::SerdeDeCustom
    }
}

impl Display for Error {
    fn fmt(&self, _f: &mut core::fmt::Formatter<'_>) -> core::fmt::Result {
        todo!()
    }
}

impl ser::StdError for Error {}

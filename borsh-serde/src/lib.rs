#![cfg_attr(not(any(feature = "std", test)), no_std)]

pub mod de;
pub mod error;
pub mod ser;

pub use error::{Error, Result};

pub use ser::to_slice;
pub use ser::to_writer;

#[cfg(test)]
mod test {
    #[test]
    fn test1() {
        let x = u32::MAX;
        let mut buf = [0u8; 5];
        let _used = crate::to_slice(&x, &mut buf).unwrap();
        //let deser: u32 = crate::from_bytes(used).unwrap();
    }
}

use crate::{Error, Result};
use serde::Serialize;

pub mod flavors;
mod serializers;

use flavors::{Flavor, Slice, Writer};
pub use serializers::Serializer;

use nlighten_io::Write;

pub fn to_slice<'a, 'b, T>(value: &'b T, buf: &'a mut [u8]) -> Result<&'a mut [u8]>
where
    T: Serialize,
{
    serialize_with_flavor::<T, Slice<'a>, &'a mut [u8]>(value, Slice::new(buf))
}

pub fn to_writer<T, W>(value: &T, writer: &mut W) -> Result<()>
where
    T: Serialize,
    W: Write,
{
    serialize_with_flavor::<T, Writer<'_, W>, ()>(value, Writer::new(writer))
}

pub fn serialize_with_flavor<T, S, O>(value: &T, storage: S) -> Result<O>
where
    T: Serialize + ?Sized,
    S: Flavor<Output = O>,
{
    let mut serializer = Serializer { output: storage };
    value.serialize(&mut serializer)?;
    serializer
        .output
        .finalize()
        .map_err(|_| Error::SerializeBufferFull)
}

#[cfg(test)]
mod test {

    use super::*;

    #[test]
    fn ser_u8() {
        let mut buf = [0_u8; 4];
        let buf = to_slice(&7_u8, &mut buf).unwrap();
        assert_eq!([7_u8], buf);
    }

    #[test]
    fn ser_u16() {
        let mut buf = [0_u8; 4];
        let buf = to_slice(&0x6240_u16, &mut buf).unwrap();
        assert_eq!([0x40_u8, 0x62_u8], buf);
    }

    #[test]
    fn ser_unit_enum() {
        #[derive(Debug, PartialEq, Eq, serde_derive::Serialize, serde_derive::Deserialize)]
        enum MyEnum {
            One,
            Two,
            Three,
        }

        let v = MyEnum::Two;
        let mut buf = [0_u8; 16];
        let buf = to_slice(&v, &mut buf).unwrap();
        assert_eq!([0x1], buf);
    }
}

use serde::{ser, Serialize};

use crate::error::{Error, Result};

use super::flavors::Flavor;

pub struct Serializer<F>
where
    F: Flavor,
{
    /// This is the Flavor(s) that will be used to modify or store any bytes generated
    /// by serialization
    pub output: F,
}

impl<'a, F> ser::Serializer for &'a mut Serializer<F>
where
    F: Flavor,
{
    type Ok = ();

    type Error = Error;

    type SerializeSeq = Self;
    type SerializeTuple = Self;
    type SerializeTupleStruct = Self;
    type SerializeTupleVariant = Self;
    type SerializeMap = Self;
    type SerializeStruct = Self;
    type SerializeStructVariant = Self;

    #[inline]
    fn is_human_readable(&self) -> bool {
        false
    }

    fn serialize_bool(self, v: bool) -> Result<()> {
        self.serialize_u8(if v { 1 } else { 0 })
    }

    fn serialize_i8(self, v: i8) -> core::result::Result<Self::Ok, Self::Error> {
        self.output.try_extend(&v.to_le_bytes())
    }

    fn serialize_i16(self, v: i16) -> core::result::Result<Self::Ok, Self::Error> {
        self.output.try_extend(&v.to_le_bytes())
    }

    fn serialize_i32(self, v: i32) -> core::result::Result<Self::Ok, Self::Error> {
        self.output.try_extend(&v.to_le_bytes())
    }

    fn serialize_i64(self, v: i64) -> core::result::Result<Self::Ok, Self::Error> {
        self.output.try_extend(&v.to_le_bytes())
    }

    fn serialize_u8(self, v: u8) -> core::result::Result<Self::Ok, Self::Error> {
        self.output
            .try_push(v)
            .map_err(|_| Error::SerializeBufferFull)
    }

    fn serialize_u16(self, v: u16) -> core::result::Result<Self::Ok, Self::Error> {
        self.output.try_extend(&v.to_le_bytes())
    }

    fn serialize_u32(self, v: u32) -> core::result::Result<Self::Ok, Self::Error> {
        self.output.try_extend(&v.to_le_bytes())
    }

    fn serialize_u64(self, v: u64) -> core::result::Result<Self::Ok, Self::Error> {
        self.output.try_extend(&v.to_le_bytes())
    }

    fn serialize_f32(self, v: f32) -> core::result::Result<Self::Ok, Self::Error> {
        if v.is_nan() {
            return Err(Error::NaNNotAllowed);
        }
        self.output.try_extend(&v.to_le_bytes())
    }

    fn serialize_f64(self, v: f64) -> core::result::Result<Self::Ok, Self::Error> {
        if v.is_nan() {
            return Err(Error::NaNNotAllowed);
        }
        self.output.try_extend(&v.to_le_bytes())
    }

    fn serialize_char(self, v: char) -> core::result::Result<Self::Ok, Self::Error> {
        let mut buf = [0u8; 4];
        let strsl = v.encode_utf8(&mut buf);
        strsl.serialize(self)
    }

    fn serialize_str(self, v: &str) -> core::result::Result<Self::Ok, Self::Error> {
        self.serialize_bytes(v.as_bytes())
    }

    fn serialize_bytes(self, v: &[u8]) -> core::result::Result<Self::Ok, Self::Error> {
        let len = v.len() as u32;
        self.serialize_u32(len)
            .map_err(|_| Error::SerializeBufferFull)?;
        self.output.try_extend(v)
    }

    fn serialize_none(self) -> core::result::Result<Self::Ok, Self::Error> {
        self.serialize_u8(0)
    }

    fn serialize_some<T: ?Sized>(self, value: &T) -> core::result::Result<Self::Ok, Self::Error>
    where
        T: Serialize,
    {
        self.serialize_u8(1)?;
        value.serialize(self)
    }

    fn serialize_unit(self) -> core::result::Result<Self::Ok, Self::Error> {
        Ok(())
    }

    fn serialize_unit_struct(
        self,
        _name: &'static str,
    ) -> core::result::Result<Self::Ok, Self::Error> {
        Ok(())
    }

    fn serialize_unit_variant(
        self,
        _name: &'static str,
        variant_index: u32,
        _variant: &'static str,
    ) -> core::result::Result<Self::Ok, Self::Error> {
        self.serialize_u8(
            variant_index
                .try_into()
                .map_err(|_| Error::VariantIndexTooBig)?,
        )
    }

    fn serialize_newtype_struct<T: ?Sized>(
        self,
        _name: &'static str,
        value: &T,
    ) -> core::result::Result<Self::Ok, Self::Error>
    where
        T: Serialize,
    {
        value.serialize(self)
    }

    fn serialize_newtype_variant<T: ?Sized>(
        self,
        _name: &'static str,
        variant_index: u32,
        _variant: &'static str,
        value: &T,
    ) -> core::result::Result<Self::Ok, Self::Error>
    where
        T: Serialize,
    {
        self.serialize_u8(
            variant_index
                .try_into()
                .map_err(|_| Error::VariantIndexTooBig)?,
        )?;
        value.serialize(self)
    }

    fn serialize_seq(
        self,
        len: Option<usize>,
    ) -> core::result::Result<Self::SerializeSeq, Self::Error> {
        match len {
            Some(len) => {
                self.serialize_u32(len as u32)?;
                Ok(self)
            }
            None => Err(Error::UnknownSequenceLength),
        }
    }

    fn serialize_tuple(
        self,
        _len: usize,
    ) -> core::result::Result<Self::SerializeTuple, Self::Error> {
        Ok(self)
    }

    fn serialize_tuple_struct(
        self,
        _name: &'static str,
        _len: usize,
    ) -> core::result::Result<Self::SerializeTupleStruct, Self::Error> {
        Ok(self)
    }

    fn serialize_tuple_variant(
        self,
        _name: &'static str,
        variant_index: u32,
        _variant: &'static str,
        _len: usize,
    ) -> core::result::Result<Self::SerializeTupleVariant, Self::Error> {
        self.serialize_u8(
            variant_index
                .try_into()
                .map_err(|_| Error::VariantIndexTooBig)?,
        )?;
        Ok(self)
    }

    fn serialize_map(
        self,
        len: Option<usize>,
    ) -> core::result::Result<Self::SerializeMap, Self::Error> {
        match len {
            Some(len) => {
                self.serialize_u32(len as u32)?;
                Ok(self)
            }
            None => Err(Error::UnknownSequenceLength),
        }
    }

    fn serialize_struct(
        self,
        _name: &'static str,
        _len: usize,
    ) -> core::result::Result<Self::SerializeStruct, Self::Error> {
        Ok(self)
    }

    fn serialize_struct_variant(
        self,
        _name: &'static str,
        variant_index: u32,
        _variant: &'static str,
        _len: usize,
    ) -> core::result::Result<Self::SerializeStructVariant, Self::Error> {
        self.serialize_u8(
            variant_index
                .try_into()
                .map_err(|_| Error::VariantIndexTooBig)?,
        )?;
        Ok(self)
    }

    fn collect_str<T: ?Sized>(self, value: &T) -> Result<Self::Ok>
    where
        T: core::fmt::Display,
    {
        use core::fmt::Write;

        // Unfortunately, we need to know the size of the serialized data before
        // we can place it into the output. In order to do this, we run the formatting
        // of the output data TWICE, the first time to determine the length, the
        // second time to actually format the data
        //
        // There are potentially other ways to do this, such as:
        //
        // * Reserving a fixed max size, such as 5 bytes, for the length field, and
        //     leaving non-canonical trailing zeroes at the end. This would work up
        //     to some reasonable length, but might have some portability vs max size
        //     tradeoffs, e.g. 64KiB if we pick 3 bytes, or 4GiB if we pick 5 bytes
        // * Expose some kind of "memmove" capability to flavors, to allow us to
        //     format into the buffer, then "scoot over" that many times.
        //
        // Despite the current approaches downside in speed, it is likely flexible
        // enough for the rare-ish case where formatting a Debug impl is necessary.
        // This is better than the previous panicking behavior, and can be improved
        // in the future.
        struct CountWriter {
            ct: usize,
        }
        impl Write for CountWriter {
            fn write_str(&mut self, s: &str) -> core::result::Result<(), core::fmt::Error> {
                self.ct += s.as_bytes().len();
                Ok(())
            }
        }

        let mut ctr = CountWriter { ct: 0 };

        // This is the first pass through, where we just count the length of the
        // data that we are given
        write!(&mut ctr, "{}", value).map_err(|_| Error::CollectStrError)?;
        let len = ctr.ct;

        self.output
            .try_extend(&len.to_le_bytes())
            .map_err(|_| Error::SerializeBufferFull)?;

        struct FmtWriter<'a, IF>
        where
            IF: Flavor,
        {
            output: &'a mut IF,
        }
        impl<'a, IF> Write for FmtWriter<'a, IF>
        where
            IF: Flavor,
        {
            fn write_str(&mut self, s: &str) -> core::result::Result<(), core::fmt::Error> {
                self.output
                    .try_extend(s.as_bytes())
                    .map_err(|_| core::fmt::Error::default())
            }
        }

        // This second pass actually inserts the data.
        let mut fw = FmtWriter {
            output: &mut self.output,
        };
        write!(&mut fw, "{}", value).map_err(|_| Error::CollectStrError)?;

        Ok(())
    }
}

impl<'a, F> ser::SerializeSeq for &'a mut Serializer<F>
where
    F: Flavor,
{
    // Must match the `Ok` type of the serializer.
    type Ok = ();
    // Must match the `Error` type of the serializer.
    type Error = Error;

    // Serialize a single element of the sequence.
    #[inline]
    fn serialize_element<T>(&mut self, value: &T) -> Result<()>
    where
        T: ?Sized + Serialize,
    {
        value.serialize(&mut **self)
    }

    // Close the sequence.
    #[inline]
    fn end(self) -> Result<()> {
        Ok(())
    }
}

impl<'a, F> ser::SerializeTuple for &'a mut Serializer<F>
where
    F: Flavor,
{
    type Ok = ();
    type Error = Error;

    #[inline]
    fn serialize_element<T>(&mut self, value: &T) -> Result<()>
    where
        T: ?Sized + Serialize,
    {
        value.serialize(&mut **self)
    }

    #[inline]
    fn end(self) -> Result<()> {
        Ok(())
    }
}

impl<'a, F> ser::SerializeTupleStruct for &'a mut Serializer<F>
where
    F: Flavor,
{
    type Ok = ();
    type Error = Error;

    #[inline]
    fn serialize_field<T>(&mut self, value: &T) -> Result<()>
    where
        T: ?Sized + Serialize,
    {
        value.serialize(&mut **self)
    }

    #[inline]
    fn end(self) -> Result<()> {
        Ok(())
    }
}

impl<'a, F> ser::SerializeTupleVariant for &'a mut Serializer<F>
where
    F: Flavor,
{
    type Ok = ();
    type Error = Error;

    #[inline]
    fn serialize_field<T>(&mut self, value: &T) -> Result<()>
    where
        T: ?Sized + Serialize,
    {
        value.serialize(&mut **self)
    }

    #[inline]
    fn end(self) -> Result<()> {
        Ok(())
    }
}

impl<'a, F> ser::SerializeMap for &'a mut Serializer<F>
where
    F: Flavor,
{
    type Ok = ();
    type Error = Error;

    #[inline]
    fn serialize_key<T>(&mut self, key: &T) -> Result<()>
    where
        T: ?Sized + Serialize,
    {
        key.serialize(&mut **self)
    }

    #[inline]
    fn serialize_value<T>(&mut self, value: &T) -> Result<()>
    where
        T: ?Sized + Serialize,
    {
        value.serialize(&mut **self)
    }

    #[inline]
    fn end(self) -> Result<()> {
        Ok(())
    }
}

impl<'a, F> ser::SerializeStruct for &'a mut Serializer<F>
where
    F: Flavor,
{
    type Ok = ();
    type Error = Error;

    #[inline]
    fn serialize_field<T>(&mut self, _key: &'static str, value: &T) -> Result<()>
    where
        T: ?Sized + Serialize,
    {
        value.serialize(&mut **self)
    }

    #[inline]
    fn end(self) -> Result<()> {
        Ok(())
    }
}

impl<'a, F> ser::SerializeStructVariant for &'a mut Serializer<F>
where
    F: Flavor,
{
    type Ok = ();
    type Error = Error;

    #[inline]
    fn serialize_field<T>(&mut self, _key: &'static str, value: &T) -> Result<()>
    where
        T: ?Sized + Serialize,
    {
        value.serialize(&mut **self)
    }

    #[inline]
    fn end(self) -> Result<()> {
        Ok(())
    }
}

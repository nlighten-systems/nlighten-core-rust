use crate::{Read, Result, Write};

pub struct Cursor<T> {
    inner: T,
    pos: usize,
}

impl<T> Cursor<T> {
    pub const fn new(inner: T) -> Self {
        Self { inner, pos: 0 }
    }

    pub fn into_inner(self) -> T {
        self.inner
    }
}

impl<T> Cursor<T>
where
    T: AsRef<[u8]>,
{
    pub fn remaining_slice(&self) -> &[u8] {
        let len = self.pos.min(self.inner.as_ref().len());
        &self.inner.as_ref()[len..]
    }
}

impl<T> Read for Cursor<T>
where
    T: AsRef<[u8]>,
{
    fn read(&mut self, buf: &mut [u8]) -> Result<usize> {
        let n = Read::read(&mut self.remaining_slice(), buf)?;
        self.pos += n;
        Ok(n)
    }

    fn read_exact(&mut self, buf: &mut [u8]) -> Result<()> {
        let n = buf.len();
        Read::read_exact(&mut self.remaining_slice(), buf)?;
        self.pos += n;
        Ok(())
    }
}

impl<T> Write for Cursor<T>
where
    T: AsMut<[u8]>,
{
    fn write(&mut self, buf: &[u8]) -> Result<usize> {
        let mut slice = &mut self.inner.as_mut()[self.pos..];
        let n = Write::write(&mut slice, buf)?;
        self.pos += n;
        Ok(n)
    }

    fn flush(&mut self) -> Result<()> {
        Ok(())
    }
}

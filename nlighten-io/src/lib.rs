#![cfg_attr(not(any(feature = "std", test)), no_std)]
#![cfg_attr(feature = "async", feature(async_fn_in_trait))]

mod cursor;
mod error;
mod readext;
mod writeext;

pub use cursor::Cursor;
pub use error::Error;
pub use readext::ReadExt;
pub use writeext::WriteExt;

pub type Result<T> = core::result::Result<T, Error>;

pub trait Read {
    fn read(&mut self, buf: &mut [u8]) -> Result<usize>;
    fn read_exact(&mut self, buf: &mut [u8]) -> Result<()> {
        default_read_exact(self, buf)
    }
}

fn default_read_exact<R>(this: &mut R, mut buf: &mut [u8]) -> Result<()>
where
    R: Read + ?Sized,
{
    while !buf.is_empty() {
        match this.read(buf) {
            Ok(0) => break,
            Ok(n) => {
                buf = &mut buf[n..];
            }
            Err(e) => return Err(e),
        }
    }
    Ok(())
}

pub trait Write {
    fn write(&mut self, buf: &[u8]) -> Result<usize>;
    fn flush(&mut self) -> Result<()> {
        Ok(())
    }
    fn write_all(&mut self, buf: &[u8]) -> Result<()> {
        default_write_all(self, buf)
    }
}

fn default_write_all<W>(this: &mut W, mut buf: &[u8]) -> Result<()>
where
    W: Write + ?Sized,
{
    while !buf.is_empty() {
        match this.write(buf) {
            Ok(0) => break,
            Ok(n) => {
                buf = &buf[n..];
            }
            Err(e) => return Err(e),
        }
    }
    Ok(())
}

impl Read for &[u8] {
    fn read(&mut self, buf: &mut [u8]) -> Result<usize> {
        let n = self.len().min(buf.len());
        unsafe {
            core::ptr::copy_nonoverlapping(self.as_ptr(), buf.as_mut_ptr(), n);
        }
        Ok(n)
    }

    fn read_exact(&mut self, buf: &mut [u8]) -> Result<()> {
        let n = self.read(buf)?;
        if n != buf.len() {
            Err(crate::error::Error::InvalidArg)
        } else {
            Ok(())
        }
    }
}

#[cfg(not(feature = "std"))]
impl Write for &mut [u8] {
    fn write(&mut self, buf: &[u8]) -> Result<usize> {
        let n = self.len().min(buf.len());
        unsafe {
            core::ptr::copy_nonoverlapping(buf.as_ptr(), self.as_mut_ptr(), n);
        }
        Ok(n)
    }
}

#[cfg(all(feature = "heapless", not(feature = "std")))]
impl<const N: usize> Write for heapless::Vec<u8, N> {
    fn write(&mut self, buf: &[u8]) -> Result<usize> {
        self.extend_from_slice(buf)
            .map_err(|_| crate::error::Error::TooBig)?;
        Ok(buf.len())
    }
}

#[cfg(feature = "std")]
impl<T> Write for T
where
    T: std::io::Write,
{
    fn write(&mut self, buf: &[u8]) -> Result<usize> {
        std::io::Write::write(self, buf).map_err(|_| Error::IO)
    }

    fn flush(&mut self) -> Result<()> {
        std::io::Write::flush(self).map_err(|_| Error::IO)
    }

    fn write_all(&mut self, buf: &[u8]) -> Result<()> {
        std::io::Write::write_all(self, buf).map_err(|_| Error::IO)
    }
}

#[cfg(feature = "async")]
mod asynch {

    use super::*;

    pub trait AsyncWrite {
        async fn write(&mut self, buf: &[u8]) -> Result<usize>;
        async fn flush(&mut self) -> Result<()> {
            Ok(())
        }

        async fn write_all(&mut self, buf: &[u8]) -> Result<()> {
            let mut buf = buf;
            while !buf.is_empty() {
                match self.write(buf).await {
                    Ok(0) => return Err(crate::error::Error::WriteZero),
                    Ok(n) => buf = &buf[n..],
                    Err(e) => return Err(e),
                }
            }
            Ok(())
        }
    }

    pub trait AsyncRead {
        async fn read(&mut self, buf: &mut [u8]) -> Result<usize>;
        async fn read_exact(&mut self, mut buf: &mut [u8]) -> Result<()> {
            while !buf.is_empty() {
                match self.read(buf).await {
                    Ok(0) => break,
                    Ok(n) => buf = &mut buf[n..],
                    Err(e) => return Err(e),
                }
            }
            Ok(())
        }
    }
}

#[cfg(feature = "async")]
pub use asynch::*;

use super::Read;
use super::Result;

pub trait ReadExt: Read {
    fn read_u8(&mut self) -> Result<u8> {
        let mut buf = [0];
        self.read_exact(&mut buf)?;
        Ok(buf[0])
    }

    fn read_u16(&mut self) -> Result<u16> {
        let mut buf = [0, 0];
        self.read_exact(&mut buf)?;
        Ok(u16::from_be_bytes(buf))
    }

    fn read_u32(&mut self) -> Result<u32> {
        let mut buf = [0, 0, 0, 0];
        self.read_exact(&mut buf)?;
        Ok(u32::from_be_bytes(buf))
    }
}

impl<T> ReadExt for T where T: Read {}

use super::Write;

pub trait WriteExt: Write {
    fn write_u8(&mut self, v: u8) {
        let buf = [v];
        assert!(self.write_all(&buf).is_ok());
    }

    fn write_u16(&mut self, v: u16) {
        assert!(self.write_all(&v.to_be_bytes()).is_ok());
    }

    fn write_u32(&mut self, v: u32) {
        assert!(self.write_all(&v.to_be_bytes()).is_ok());
    }

    fn write_slice<T>(&mut self, src: T)
    where
        T: AsRef<[u8]>,
    {
        assert!(self.write_all(src.as_ref()).is_ok())
    }
}

impl<T> WriteExt for T where T: Write {}

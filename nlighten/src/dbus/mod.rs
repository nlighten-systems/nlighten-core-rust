use crate::{
    lora::CardId,
    types::{FullRxPkt, RxPktSig, TxPkt},
};

use serde::{Deserialize, Serialize};
use zbus::{dbus_proxy, zvariant::Type};

macro_rules! dbus_opaque {
    ($name:ident, $t:ty) => {
        #[derive(Debug, Serialize, Deserialize, Type)]
        pub struct $name(Vec<u8>);

        impl $name {
            pub fn wrap(value: &$t) -> Self {
                let mut buf = Vec::new();
                borsh_serde::to_writer(&value, &mut buf).expect("no io error");
                Self(buf)
            }

            pub fn unwrap(&self) -> Result<$t, borsh_serde::Error> {
                borsh_serde::de::from_bytes(&self.0)
            }
        }
    };
}

#[derive(Serialize, Deserialize, Type)]
pub enum SendResult {
    Ok,
    ErrQueueFull,
    ErrPacketCollision,
    ErrTooLate,
    ErrTooEarly,
    ErrIO,
}

#[dbus_proxy(
    interface = "com.nlighten.LoraCard1",
    default_service = "com.nlighten.LoraCard",
    default_path = "/com/nlighten/LoraCard"
)]
trait LoraCard {
    fn send(&self, pkt: DBusTxPkt) -> zbus::fdo::Result<SendResult>;

    fn sign(&self, data: Vec<u8>) -> zbus::fdo::Result<Vec<u8>>;

    #[dbus_proxy(signal)]
    fn rx(&self, rx: DBusFullRxPkt) -> zbus::Result<()>;

    #[dbus_proxy(signal)]
    fn rx_sig(&self, rx: DBusRxSig) -> zbus::Result<()>;

    #[dbus_proxy(property)]
    fn eui(&self) -> zbus::fdo::Result<CardId>;

    #[dbus_proxy(property)]
    fn pub_key(&self) -> zbus::fdo::Result<Vec<u8>>;

    #[dbus_proxy(property)]
    fn firmware(&self) -> zbus::fdo::Result<u32>;

    #[dbus_proxy(property)]
    fn serial_num(&self) -> zbus::fdo::Result<u32>;
}

dbus_opaque!(DBusRxSig, RxPktSig);
dbus_opaque!(DBusFullRxPkt, FullRxPkt);
dbus_opaque!(DBusTxPkt, TxPkt);

use core::{
    fmt::{self, Display},
    time::Duration,
};

use chrono::prelude::*;

//use chrono::{Utc, DateTime, NaiveDateTime};
use serde::{Deserialize, Serialize};

const SECS_PER_WEEK: u64 = 60 * 60 * 24 * 7;
const NANOS_PER_SEC: u64 = 1_000_000_000;

const GPS_UNIX_DIFF: u64 = 315964800 * NANOS_PER_SEC;

/// GPS time is defined as the amount of time since 1980-01-06 0:00 UTC (the GPS epoch).
/// GPS time is a continuous time scale and does not adjust for leap second insertions.
/// It therefore diverges from UTC at the introduction of each leap second.
#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Serialize, Deserialize)]
#[cfg_attr(feature = "dbus", derive(zvariant::Type))]
pub struct GPSTime(u64);

impl GPSTime {
    //pub const GPS_EPOCH: OffsetDateTime = datetime!(1980-01-06 0:00 UTC);

    pub const fn new(week: u16, secs: u32, nanos: u32) -> Self {
        let mut nanos = nanos as u64;
        nanos += secs as u64 * NANOS_PER_SEC;
        nanos += week as u64 * SECS_PER_WEEK * NANOS_PER_SEC;
        Self(nanos)
    }

    pub const fn from_seconds(secs: u32) -> Self {
        Self(secs as u64 * NANOS_PER_SEC)
    }

    /// create a new GPSTime from unix timestamp (nanoseconds since unix epoch)
    pub const fn from_unix_timestamp(timestamp: u64) -> Self {
        let nanos = timestamp - GPS_UNIX_DIFF;
        Self(nanos)
    }

    /// unix timestamp (nanoseconds since unix epoch)
    pub fn to_unix_timestamp(&self) -> u64 {
        self.0 + GPS_UNIX_DIFF
    }

    pub fn as_datetime(&self) -> NaiveDateTime {
        let timestamp = self.to_unix_timestamp();
        let secs = timestamp / NANOS_PER_SEC;
        let nanos = timestamp % NANOS_PER_SEC;
        NaiveDateTime::from_timestamp_opt(secs as i64, nanos as u32).unwrap()
    }

    pub fn as_utc(&self) -> DateTime<Utc> {
        let time_date = self.as_datetime();
        DateTime::from_utc(time_date, Utc)
    }

    pub fn as_duration(&self) -> Duration {
        Duration::from_nanos(self.0)
    }

    pub fn saturating_add_nanos(self, nanos: i64) -> GPSTime {
        Self(self.0.saturating_add_signed(nanos))
    }

    /// Whole number of seconds
    pub fn secs(&self) -> u64 {
        self.0 / NANOS_PER_SEC
    }

    /// Get the number of nanoseconds past the number of whole seconds.
    pub fn subsec_nanos(&self) -> u64 {
        self.0 % NANOS_PER_SEC
    }

    pub fn whole_nanoseconds(&self) -> u64 {
        self.0
    }
}

impl Display for GPSTime {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let datetime = self.as_datetime();
        datetime.fmt(f)
    }
}

#[cfg(feature = "defmt")]
impl defmt::Format for GPSTime {
    fn format(&self, f: defmt::Formatter) {
        defmt::write!(f, "{}.{}", self.secs(), self.subsec_nanos());
    }
}
#[derive(Debug, Clone, Copy, PartialEq, Eq, Serialize, Deserialize)]
#[cfg_attr(feature = "dbus", derive(zvariant::Type))]
pub struct WGS84Position {
    /// longitude (deg) 1e-7 scale
    pub lon: i32,

    /// latitude (deg) 1e-7 scale
    pub lat: i32,

    /// Height above ellipsoid (mm)
    pub height: i32,

    /// Horizontal accuracy estimate (mm)
    pub hacc: u32,

    /// Vertical accuracy estimate (mm)
    pub vacc: Option<u32>,
}

impl WGS84Position {
    /// returns latitude and longitude in degrees
    pub fn to_lat_lon(&self) -> (f64, f64) {
        fn to_f64(v: i32) -> f64 {
            v as f64 * 1e-7_f64
        }

        (to_f64(self.lat), to_f64(self.lon))
    }

    /// return geodetic coordinates (lat (rad), lon (rad), altitude (m))
    /// https://en.wikipedia.org/wiki/Geographic_coordinate_system
    pub fn to_geodetic(&self) -> (f64, f64, f64) {
        let (lat, lon) = self.to_lat_lon();
        (
            lat.to_radians(),
            lon.to_radians(),
            self.height as f64 * 1e-3_f64,
        )
    }
}

#[cfg(feature = "defmt")]
impl ::defmt::Format for WGS84Position {
    fn format(&self, f: defmt::Formatter) {
        fn to_decimal(value: i32) -> (i32, i32) {
            (value / 10_000_000_i32, value.abs() % 10_000_000_i32)
        }

        let lat = to_decimal(self.lat);
        let lon = to_decimal(self.lon);
        defmt::write!(f, "[{}.{:05}, {}.{:05}]", lat.0, lat.1, lon.0, lon.1)
    }
}

pub struct Position3D {
    pub x: f32,
    pub y: f32,
    pub z: f32,
}

impl Display for WGS84Position {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        fn to_decimal(value: i32) -> (i32, i32) {
            (value / 10_000_000_i32, value.abs() % 10_000_000_i32)
        }

        let lat = to_decimal(self.lat);
        let lon = to_decimal(self.lon);
        write!(f, "[{}.{:05}, {}.{:05}]", lat.0, lat.1, lon.0, lon.1)
    }
}

#[cfg(test)]
mod test {
    #![allow(unused_imports)]
    use super::*;

    #[test]
    fn test_gpstime_display() {
        let gps_time = GPSTime::new(2266, 264378, 23498725);
        let display_str = gps_time.to_string();
        assert_eq!("2023-06-14 01:26:18.023498725", display_str);
    }

    #[test]
    fn from_timestamp_convert() {
        let gps_time = GPSTime::new(2000, 300, 23498725);
        let ts = gps_time.to_unix_timestamp();
        assert_eq!(gps_time, GPSTime::from_unix_timestamp(ts));
    }

    #[test]
    fn from_timestamp() {
        let timestamp = 1640995200;
        let utc_time = Utc.timestamp_opt(timestamp, 0).unwrap();
        assert_eq!(Utc.with_ymd_and_hms(2022, 1, 1, 0, 0, 0).unwrap(), utc_time);
        let gps_time = GPSTime::from_unix_timestamp(timestamp as u64 * NANOS_PER_SEC);
        assert_eq!(utc_time, gps_time.as_utc());
    }
}

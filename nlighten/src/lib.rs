#![cfg_attr(not(any(feature = "std", test)), no_std)]
#![cfg_attr(feature = "async", feature(async_fn_in_trait))]

#[cfg(feature = "alloc")]
extern crate alloc;

pub use nlighten_io::Error;
pub mod io {
    pub use nlighten_io::*;
}
pub mod frame;
pub mod gps;
pub mod lora;
pub mod protocol;
pub mod serialnum;
pub mod types;
mod utctime;
pub mod version_number;

#[cfg(feature = "dbus")]
pub mod dbus;

mod lib {
    pub use crate::Error;
    pub type SmallString = heapless::String<16>;

    #[cfg(feature = "crypto")]
    pub use ed25519_compact::*;

    #[cfg(feature = "alloc")]
    pub use alloc::{
        boxed::Box,
        string::{String, ToString},
    };

    pub use crate::utctime::{Time, UTCTime};
}

pub mod prelude {
    pub use crate::lib::*;
}

use core::fmt::Display;
use serde::{Deserialize, Serialize};

/// CardId is a 8-byte value that uniquely uniqually identifys the Secure Concentrator
/// It is the same as the SX130x unique id

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash, Serialize, Deserialize)]
#[cfg_attr(feature = "dbus", derive(zvariant::Type))]
pub struct CardId([u8; 8]);

impl CardId {
    pub fn new(v: [u8; 8]) -> Self {
        Self(v)
    }
}

impl AsRef<[u8]> for CardId {
    fn as_ref(&self) -> &[u8] {
        &self.0
    }
}

impl Display for CardId {
    fn fmt(&self, f: &mut core::fmt::Formatter<'_>) -> core::fmt::Result {
        write!(
            f,
            "{:02x}:{:02x}:{:02x}:{:02x}:{:02x}:{:02x}:{:02x}:{:02x}",
            self.0[0], self.0[1], self.0[2], self.0[3], self.0[4], self.0[5], self.0[6], self.0[7]
        )
    }
}

#[cfg(feature = "dbus")]
impl TryFrom<zvariant::OwnedValue> for CardId {
    type Error = zvariant::Error;

    fn try_from(value: zvariant::OwnedValue) -> Result<Self, Self::Error> {
        let v: Vec<u8> = value.try_into()?;
        Ok(CardId::new(
            v.as_slice()
                .try_into()
                .map_err(|_| zvariant::Error::IncorrectType)?,
        ))
    }
}

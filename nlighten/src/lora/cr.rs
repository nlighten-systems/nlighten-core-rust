use crate::Error;

use core::str::FromStr;

pub use int_enum::IntEnum;
use serde::{de::Visitor, Deserialize, Serialize};

#[allow(non_camel_case_types)]
#[repr(u8)]
#[derive(Debug, IntEnum, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
#[cfg_attr(feature = "dbus", derive(zvariant::Type), zvariant(signature = "s"))]
pub enum CodingRate {
    OFF = 0,
    CR_4_5 = 1,
    CR_4_6 = 2,
    CR_4_7 = 3,
    CR_4_8 = 4,
}

impl FromStr for CodingRate {
    type Err = Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s {
            "OFF" => Ok(CodingRate::OFF),
            "4/5" => Ok(CodingRate::CR_4_5),
            "4/6" => Ok(CodingRate::CR_4_6),
            "2/3" => Ok(CodingRate::CR_4_6),
            "4/7" => Ok(CodingRate::CR_4_7),
            "4/8" => Ok(CodingRate::CR_4_8),
            "1/2" => Ok(CodingRate::CR_4_8),
            _ => Err(Error::InvalidArg),
        }
    }
}

impl Serialize for CodingRate {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: serde::Serializer,
    {
        let str_rep = match self {
            CodingRate::OFF => "OFF",
            CodingRate::CR_4_5 => "4/5",
            CodingRate::CR_4_6 => "4/6",
            CodingRate::CR_4_7 => "4/7",
            CodingRate::CR_4_8 => "4/8",
        };
        serializer.serialize_str(str_rep)
    }
}

impl<'de> Deserialize<'de> for CodingRate {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: serde::Deserializer<'de>,
    {
        struct MyVisitor;
        impl<'de> Visitor<'de> for MyVisitor {
            type Value = CodingRate;

            fn expecting(&self, formatter: &mut core::fmt::Formatter) -> core::fmt::Result {
                write!(formatter, "a string representing the coding rate")
            }

            fn visit_str<E>(self, v: &str) -> Result<Self::Value, E>
            where
                E: serde::de::Error,
            {
                CodingRate::from_str(v).map_err(|e| E::custom(format_args!("invalid: {}", e)))
            }
        }

        deserializer.deserialize_str(MyVisitor)
    }
}

use core::{fmt::Display, str::FromStr};

use super::{Bandwidth, SpreadingFactor};

use crate::prelude::*;

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
#[cfg_attr(feature = "dbus", derive(zvariant::Type), zvariant(signature = "s"))]
pub struct Datarate(pub SpreadingFactor, pub Bandwidth);

impl Datarate {
    pub fn to_string(&self) -> SmallString {
        use core::fmt::Write;

        let mut fmt = SmallString::new();
        let _ = write!(&mut fmt, "{:?}{:?}", self.0, self.1);
        fmt
    }

    #[inline]
    pub fn sf(&self) -> SpreadingFactor {
        self.0
    }

    #[inline]
    pub fn bw(&self) -> Bandwidth {
        self.1
    }
}

impl FromStr for Datarate {
    type Err = crate::Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        use safe_regex::{regex, Matcher2};
        let matcher: Matcher2<_> = regex!(br"SF([0-9]+)BW([1|2|5|0]{3})");
        match matcher.match_slices(s.as_bytes()) {
            Some((sf, bw)) => {
                use core::str;
                use int_enum::IntEnum;

                if let (Ok(sf), Ok(bw)) = (
                    SpreadingFactor::from_int(unsafe {
                        str::from_utf8_unchecked(sf).parse::<u8>().unwrap()
                    }),
                    match bw {
                        br"125" => Ok(Bandwidth::BW125),
                        br"250" => Ok(Bandwidth::BW250),
                        br"500" => Ok(Bandwidth::BW500),
                        _ => Err(""),
                    },
                ) {
                    Ok(Datarate(sf, bw))
                } else {
                    Err(Error::InvalidArg)
                }
            }
            None => Err(Error::InvalidArg),
        }
    }
}

impl Display for Datarate {
    fn fmt(&self, f: &mut core::fmt::Formatter<'_>) -> core::fmt::Result {
        write!(f, "{:?}{:?}", self.0, self.1)
    }
}

#[cfg(feature = "defmt")]
impl defmt::Format for Datarate {
    fn format(&self, f: defmt::Formatter) {
        defmt::write!(f, "{}{}", self.0, self.1)
    }
}

impl serde::ser::Serialize for Datarate {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: serde::Serializer,
    {
        use core::fmt::Write;
        let mut fmt = SmallString::new();
        write!(&mut fmt, "{}", self)
            .map_err(|e| serde::ser::Error::custom(format_args!("{}", e)))?;
        serializer.serialize_str(fmt.as_str())
    }
}

impl<'de> serde::de::Deserialize<'de> for Datarate {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: serde::Deserializer<'de>,
    {
        struct MyStrVisitor;

        impl<'de> serde::de::Visitor<'de> for MyStrVisitor {
            type Value = Datarate;

            // fn expecting(&self, formatter: &mut alloc::fmt::Formatter) -> alloc::fmt::Result {
            //     formatter.write_str("a string in the from SFxxBWxxx")
            // }

            fn expecting(
                &self,
                formatter: &mut serde::__private::fmt::Formatter,
            ) -> serde::__private::fmt::Result {
                formatter.write_str("a string in the from SFxxBWxxx")
            }

            fn visit_str<E>(self, v: &str) -> Result<Self::Value, E>
            where
                E: serde::de::Error,
            {
                Datarate::from_str(v)
                    .map_err(|_| E::custom(format_args!("cannot parse {} as datarate", v)))
            }
        }

        deserializer.deserialize_str(MyStrVisitor)
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn parse_datarate() {
        let dr = Datarate::from_str("SF7BW125");
        assert_eq!(
            dr.unwrap(),
            Datarate(SpreadingFactor::SF7, Bandwidth::BW125)
        );
    }

    #[test]
    fn serialize() {
        let dr = Datarate(SpreadingFactor::SF5, Bandwidth::BW250);
        let r = serde_json::to_string(&dr);
        assert_eq!("\"SF5BW250\"", r.unwrap());
    }

    #[test]
    fn deserialize() {
        let r = serde_json::from_str::<Datarate>("\"SF12BW500\"");
        assert_eq!(
            r.unwrap(),
            Datarate(SpreadingFactor::SF12, Bandwidth::BW500)
        );
    }
}

use serde::{Deserialize, Serialize};

/// Frequency in kHz
#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash, Serialize, Deserialize)]
pub struct Frequency(u32);

impl Frequency {
    pub fn from_khz(khz: u32) -> Self {
        Self(khz)
    }

    pub fn from_hz(hz: u32) -> Self {
        Self(hz / 1000)
    }

    pub fn to_khz(&self) -> u32 {
        self.0
    }

    pub fn to_hz(&self) -> u64 {
        self.0 as u64 * 1000
    }

    pub fn to_mhz(&self) -> f32 {
        self.0 as f32 / 1000.0
    }

    /// round to 4 significant figures
    #[cfg(feature = "std")]
    pub fn round4sf(&self) -> Self {
        use rust_decimal::prelude::*;

        let freq_khz = Decimal::from(self.0);
        let freq_khz = freq_khz.round_sf(4).unwrap();
        Self(freq_khz.to_u32().unwrap())
    }
}

impl core::fmt::Display for Frequency {
    fn fmt(&self, f: &mut core::fmt::Formatter<'_>) -> core::fmt::Result {
        write!(f, "{} kHz", self.0)
    }
}

#[cfg(test)]
mod test {

    use super::*;

    #[test]
    fn test_display_ghz() {
        let freq = Frequency::from_hz(2_452_342_582);
        assert_eq!(freq.to_string(), "2452342 kHz");
    }

    #[test]
    fn test_display_mhz() {
        let freq = Frequency::from_hz(915_199_352);
        assert_eq!(freq.to_string(), "915199 kHz");
    }

    #[test]
    fn test_round_multiple_times() {
        let freq = Frequency::from_hz(2_452_342_582);
        let freq = freq.round4sf();
        let freq = freq.round4sf();
        let freq = freq.round4sf();
        assert_eq!(freq.to_hz(), 2_452_000_000);
    }

    #[test]
    fn test_round4sf_1() {
        let freq = Frequency::from_hz(2_452_342_582);
        let freq_r = freq.round4sf();
        assert_eq!(freq_r.to_hz(), 2_452_000_000);
    }

    #[test]
    fn test_round4sf_2() {
        let freq = Frequency::from_hz(915_199_352);
        let freq_r = freq.round4sf();
        assert_eq!(freq_r.to_hz(), 915_200_000);
    }

    #[test]
    fn test_round4sf_3() {
        let freq = Frequency::from_hz(199_352);
        let freq_r = freq.round4sf();
        assert_eq!(freq_r.to_hz(), 199_000);
    }
}

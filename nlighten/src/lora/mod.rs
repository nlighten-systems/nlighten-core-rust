mod bw;
pub use bw::Bandwidth;

mod cr;
pub use cr::CodingRate;

mod dr;
pub use dr::Datarate;

mod sf;
pub use sf::SpreadingFactor;

mod snr;
pub use snr::SNR;

mod rssi;
pub use rssi::RSSI;

mod cardid;
pub use cardid::CardId;

mod freq;
pub use freq::Frequency;

mod rxproof;
pub use rxproof::*;

use core::ops::Deref;
use serde::{Deserialize, Serialize};

#[cfg(feature = "crypto")]
use crate::lib::{Noise, SecretKey, Signature, SigningState, VerifyingState};

#[derive(Debug, Clone, Serialize, Deserialize)]
#[cfg_attr(feature = "dbus", derive(zvariant::Type), zvariant(signature = "ay"))]
pub struct Payload(heapless::Vec<u8, 256>);

impl Payload {
    pub fn from_slice(buf: &[u8]) -> Result<Self, ()> {
        Ok(Payload(heapless::Vec::from_slice(buf)?))
    }
}

impl Deref for Payload {
    type Target = heapless::Vec<u8, 256>;

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

impl AsRef<[u8]> for Payload {
    fn as_ref(&self) -> &[u8] {
        self.0.as_slice()
    }
}

use crate::io::Write;
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct RxPkt {
    pub rxmeta: RxMeta,
    pub payload: Payload,
}

impl RxPkt {
    pub fn write_message<T: crate::io::Write>(&self, writer: &mut T) -> crate::io::Result<()> {
        borsh_serde::ser::to_writer(self, writer).map_err(|_| crate::io::Error::IO)
    }

    #[cfg(feature = "crypto")]
    pub fn sign(&self, key: &SecretKey, noise: Noise) -> Signature {
        rxproof::sign_rxproof(&self.payload, &self.rxmeta, key, noise)
    }
}

struct SignatureWriter<T>(T);

impl Write for SignatureWriter<SigningState> {
    fn write(&mut self, buf: &[u8]) -> crate::io::Result<usize> {
        self.0.absorb(buf);
        Ok(buf.len())
    }

    fn flush(&mut self) -> crate::io::Result<()> {
        Ok(())
    }
}

impl Write for SignatureWriter<VerifyingState> {
    fn write(&mut self, buf: &[u8]) -> crate::io::Result<usize> {
        self.0.absorb(buf);
        Ok(buf.len())
    }

    fn flush(&mut self) -> crate::io::Result<()> {
        Ok(())
    }
}

impl<T> SignatureWriter<T> {
    fn into_inner(self) -> T {
        self.0
    }
}

#[cfg(test)]
mod test {

    use super::*;

    use crate::gps::{GPSTime, WGS84Position};
    #[cfg(feature = "crypto")]
    use crate::lib::{KeyPair, Seed};

    #[test]
    fn test_write_message() {
        use hex::ToHex;

        let mut buf = [0_u8; 128];

        let pkt = RxPkt {
            rxmeta: RxMeta {
                freq: Frequency::from_hz(904_000_000),
                datarate: Datarate(SpreadingFactor::SF7, Bandwidth::BW125),
                coding_rate: CodingRate::CR_4_5,
                snr: SNR::from_cdb(-1200),
                rssis: RSSI::from_cdb(100),
                tmst: 10_000,
                card_id: CardId::new([1, 2, 3, 4, 5, 6, 7, 8]),
                gps_time: Some(GPSTime::new(2000, 100, 0)),
                pos: Some(WGS84Position {
                    lat: 7353466,
                    lon: -3588727,
                    height: 38472,
                    hacc: 3425,
                    vacc: Some(683485),
                }),
            },
            payload: Payload(heapless::Vec::from_slice(b"hello world".as_slice()).unwrap()),
        };

        let buf = borsh_serde::to_slice(&pkt, &mut buf).unwrap();

        let expected = "40cb0d0008000000534637425731323503000000342f3550fb64001027000001020304050607080100e8c6d8e15cc91001893dc9ff7a34700048960000610d000001dd6d0a000b00000068656c6c6f20776f726c64";

        assert_eq!(expected, buf.encode_hex::<String>());

        let mut buf: Vec<u8> = vec![];

        pkt.write_message(&mut buf).unwrap();
        assert_eq!(expected, buf.encode_hex::<String>())
    }

    #[cfg(feature = "crypto")]
    #[test]
    fn test_sign_message() {
        let seed = Seed::from([5; 32]);
        let keypair = KeyPair::from_seed(seed);

        let pkt = RxPkt {
            rxmeta: RxMeta {
                freq: Frequency::from_hz(904_000_000),
                datarate: Datarate(SpreadingFactor::SF7, Bandwidth::BW125),
                coding_rate: CodingRate::CR_4_5,
                snr: SNR::from_cdb(-1200),
                rssis: RSSI::from_cdb(100),
                tmst: 10_000,
                card_id: CardId::new([1, 2, 3, 4, 5, 6, 7, 8]),
                gps_time: Some(GPSTime::new(2000, 100, 0)),
                pos: Some(WGS84Position {
                    lat: 7353466,
                    lon: -3588727,
                    height: 38472,
                    hacc: 3425,
                    vacc: Some(683485),
                }),
            },
            payload: Payload(heapless::Vec::from_slice(b"hello world".as_slice()).unwrap()),
        };

        let noise = Noise::new([0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15]);

        let sig = pkt.sign(&keypair.sk, noise);

        let rxproof = RxProofBuilder::new(sig)
            .with_payload(pkt.payload)
            .with_rxmeta(pkt.rxmeta)
            .build()
            .unwrap();

        assert!(rxproof.verify(&keypair.pk));

        let rxproof = rxproof.remove_payload();
        assert!(rxproof.verify(&keypair.pk));
    }
}

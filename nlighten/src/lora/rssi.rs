use core::fmt::Display;

use serde::{Deserialize, Serialize};

/// Received Signal Strength Indicator (in 0.1 dBm)
#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash, Serialize, Deserialize)]
pub struct RSSI(i16);

impl RSSI {
    pub fn from_db(db: f32) -> Self {
        Self((db * 10.0) as i16)
    }

    pub fn to_db(&self) -> f32 {
        self.0 as f32 / 10.0
    }

    pub fn from_cdb(cdb: i16) -> Self {
        Self(cdb)
    }

    pub fn to_cdb(&self) -> i16 {
        self.0
    }
}

impl Display for RSSI {
    fn fmt(&self, f: &mut core::fmt::Formatter<'_>) -> core::fmt::Result {
        write!(f, "{:.1} dBm", self.to_db())
    }
}

pub use aeshash::*;
pub use digest::Digest;

use super::Hash;

pub trait CryptoHashable {
    fn crypto_hash<D>(&self, digest: &mut D)
    where
        D: Digest;
}

pub fn aeshash_digest<H>(obj: &H) -> Hash
where
    H: CryptoHashable,
{
    let mut aeshash = AESHash::default();
    obj.crypto_hash(&mut aeshash);
    aeshash.finalize().into()
}

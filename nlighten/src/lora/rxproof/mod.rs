use ed25519_compact::{Noise, PublicKey, Signature};

use crate::prelude::SecretKey;

use self::hasher::{aeshash_digest, CryptoHashable, Digest};

use super::Payload;

mod hasher;
mod rxmeta;

pub use rxmeta::RxMeta;

pub type Hash = [u8; 16];

#[cfg(feature = "alloc")]
mod alloc {

    use super::*;
    use serde::{Deserialize, Serialize};

    const PAYLOAD_IDX: usize = 0;
    const RXMETA_IDX: usize = 1;

    #[derive(Debug, Serialize, Deserialize)]
    pub struct RxProof {
        tree: [Node; 2],

        #[serde(with = "crate::types::signature")]
        signature: Signature,
    }

    #[derive(Debug, Clone, Serialize, Deserialize)]
    enum Node {
        Null,
        Hash(Hash),
        Payload(Payload),
        RxMeta(RxMeta),
    }

    impl Default for Node {
        fn default() -> Self {
            Node::Null
        }
    }

    impl Node {
        fn hash(&self) -> Hash {
            match self {
                Node::Hash(hash) => hash.clone(),
                Node::Payload(payload) => aeshash_digest(payload),
                Node::RxMeta(rxmeta) => aeshash_digest(rxmeta),
                Node::Null => panic!("Cannot hash null node"),
            }
        }
    }

    impl RxProof {
        pub fn verify(&self, key: &PublicKey) -> bool {
            let mut verifier = key.verify_incremental(&self.signature).unwrap();
            verifier.absorb(self.tree[0].hash());
            verifier.absorb(self.tree[1].hash());
            verifier.verify().is_ok()
        }

        pub fn remove_payload(&self) -> Self {
            let tree = [Node::Hash(self.tree[0].hash()), self.tree[1].clone()];

            Self {
                tree,
                signature: self.signature,
            }
        }

        pub fn rxmeta(&self) -> Option<RxMeta> {
            match &self.tree[RXMETA_IDX] {
                Node::RxMeta(meta) => Some(meta.clone()),
                _ => None,
            }
        }

        pub fn rxmeta_hash(&self) -> Hash {
            self.tree[RXMETA_IDX].hash()
        }

        pub fn payload(&self) -> Option<Payload> {
            match &self.tree[PAYLOAD_IDX] {
                Node::Payload(payload) => Some(payload.clone()),
                _ => None,
            }
        }

        pub fn payload_hash(&self) -> Hash {
            self.tree[PAYLOAD_IDX].hash()
        }

        pub fn signature(&self) -> &Signature {
            &self.signature
        }
    }

    pub struct RxProofBuilder {
        signature: Signature,
        tree: [Node; 2],
    }

    impl RxProofBuilder {
        const PAYLOAD: usize = 0;
        const RXMETA: usize = 1;

        pub fn new(signature: Signature) -> Self {
            Self {
                signature,
                tree: [Node::Null, Node::Null],
            }
        }

        pub fn with_payload(mut self, payload: Payload) -> Self {
            self.tree[Self::PAYLOAD] = Node::Payload(payload);
            self
        }

        pub fn with_payload_hash(mut self, hash: Hash) -> Self {
            self.tree[Self::PAYLOAD] = Node::Hash(hash);
            self
        }

        pub fn with_rxmeta(mut self, meta: RxMeta) -> Self {
            self.tree[Self::RXMETA] = Node::RxMeta(meta);
            self
        }

        pub fn with_rxmeta_hash(mut self, hash: Hash) -> Self {
            self.tree[Self::RXMETA] = Node::Hash(hash);
            self
        }

        pub fn build(self) -> Result<RxProof, ()> {
            if let Node::Null = self.tree[0] {
                return Err(());
            }

            if let Node::Null = self.tree[1] {
                return Err(());
            }

            Ok(RxProof {
                signature: self.signature,
                tree: self.tree,
            })
        }
    }

    #[cfg(test)]
    mod test {
        use super::*;

        #[test]
        fn test_deserialize() {
            let json_str = r#"{"tree":[{"Hash":[141,152,185,126,139,164,250,224,126,252,121,26,190,91,22,71]},{"RxMeta":{"freq":904499,"datarate":"SF8BW125","coding_rate":"4/5","snr":1425,"rssis":-54,"tmst":132278219,"card_id":[0,22,192,1,241,86,56,65],"gps_time":1381028241165986843,"pos":{"lon":-713926098,"lat":425433830,"height":62165,"hacc":9095,"vacc":null}}}],"signature":[24,239,142,163,210,37,77,128,86,64,175,225,229,247,43,65,113,19,248,218,124,130,53,46,185,244,9,40,249,143,148,210,85,178,139,84,88,94,31,223,98,6,129,46,107,131,149,131,6,116,7,2,73,129,235,128,56,14,137,163,121,219,54,9]}"#;
            let req_body: RxProof = serde_json::from_str(json_str).unwrap();
            let rxmeta = req_body.rxmeta().unwrap();
            assert_eq!(rxmeta.tmst, 132278219);
        }
    }
}

#[cfg(feature = "alloc")]
pub use self::alloc::*;

impl CryptoHashable for Payload {
    fn crypto_hash<D>(&self, digest: &mut D)
    where
        D: Digest,
    {
        digest.update(self);
    }
}

pub fn sign_rxproof(
    payload: &Payload,
    rxmeta: &RxMeta,
    key: &SecretKey,
    noise: Noise,
) -> Signature {
    let mut signer = key.sign_incremental(noise);
    signer.absorb(aeshash_digest(payload));
    signer.absorb(aeshash_digest(rxmeta));
    signer.sign()
}

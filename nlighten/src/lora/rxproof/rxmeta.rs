use serde::{Deserialize, Serialize};

use crate::{
    gps::{GPSTime, WGS84Position},
    lora::{CardId, CodingRate, Datarate, Frequency, RSSI, SNR},
};

use super::hasher::{CryptoHashable, Digest};

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct RxMeta {
    pub freq: Frequency,
    pub datarate: Datarate,
    pub coding_rate: CodingRate,
    pub snr: SNR,
    /// Received Signal Strength Indicator (in 0.1 dBm)
    pub rssis: RSSI,
    /// 32 Mhz clock timestamp
    pub tmst: u32,
    pub card_id: CardId,
    pub gps_time: Option<GPSTime>,
    pub pos: Option<WGS84Position>,
}

impl CryptoHashable for RxMeta {
    fn crypto_hash<D>(&self, digest: &mut D)
    where
        D: Digest,
    {
        digest.update(&self.freq.to_khz().to_be_bytes());

        let sf = self.datarate.sf() as u8;
        digest.update(&sf.to_be_bytes());

        let bw = self.datarate.bw().hz();
        digest.update(&bw.to_be_bytes());

        let cr = self.coding_rate as u8;
        digest.update(&cr.to_be_bytes());

        digest.update(&self.snr.to_cdb().to_be_bytes());

        digest.update(&self.rssis.to_cdb().to_be_bytes());

        digest.update(&self.tmst.to_be_bytes());

        digest.update(self.card_id.as_ref());

        if let Some(gps_time) = &self.gps_time {
            digest.update(&gps_time.whole_nanoseconds().to_be_bytes());
        }

        if let Some(pos) = &self.pos {
            digest.update(&pos.lat.to_be_bytes());
            digest.update(&pos.lon.to_be_bytes());
            digest.update(&pos.height.to_be_bytes());
            digest.update(&pos.hacc.to_be_bytes());

            if let Some(vacc) = pos.vacc {
                digest.update(&vacc.to_be_bytes());
            }
        }
    }
}

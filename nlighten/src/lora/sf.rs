pub use int_enum::IntEnum;

#[repr(u8)]
#[derive(IntEnum, Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
#[cfg_attr(feature = "defmt", derive(::defmt::Format))]
pub enum SpreadingFactor {
    SF5 = 5,
    SF6 = 6,
    SF7 = 7,
    SF8 = 8,
    SF9 = 9,
    SF10 = 10,
    SF11 = 11,
    SF12 = 12,
}

impl SpreadingFactor {
    pub fn factor(&self) -> u8 {
        *self as u8
    }
}

impl<'de> serde::de::Deserialize<'de> for SpreadingFactor {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: serde::Deserializer<'de>,
    {
        struct MyStrVisitor;

        impl<'de> serde::de::Visitor<'de> for MyStrVisitor {
            type Value = SpreadingFactor;

            fn expecting(
                &self,
                formatter: &mut serde::__private::fmt::Formatter,
            ) -> serde::__private::fmt::Result {
                formatter.write_str("a string in the form SFxx")
            }

            fn visit_str<E>(self, v: &str) -> Result<Self::Value, E>
            where
                E: serde::de::Error,
            {
                match v {
                    "SF5" => Ok(SpreadingFactor::SF5),
                    "SF6" => Ok(SpreadingFactor::SF6),
                    "SF7" => Ok(SpreadingFactor::SF7),
                    "SF8" => Ok(SpreadingFactor::SF8),
                    "SF9" => Ok(SpreadingFactor::SF9),
                    "SF10" => Ok(SpreadingFactor::SF10),
                    "SF11" => Ok(SpreadingFactor::SF11),
                    "SF12" => Ok(SpreadingFactor::SF12),
                    o => Err(E::custom(format_args!(
                        "cannot parse: {} as Spreading Factor",
                        o
                    ))),
                }
            }
        }

        deserializer.deserialize_str(MyStrVisitor)
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_get_factor() {
        let sf = SpreadingFactor::SF10;
        assert_eq!(10, sf.factor());
    }
}

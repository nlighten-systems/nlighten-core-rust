use core::fmt::Display;

use serde::{Deserialize, Serialize};

/// Signal to Noise ratio (in 0.01 dB)
#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash, Serialize, Deserialize)]
pub struct SNR(i16);

impl SNR {
    pub fn from_cdb(cdb: i16) -> Self {
        Self(cdb)
    }

    pub fn from_db(db: f32) -> Self {
        Self((db * 100.0) as i16)
    }

    pub fn to_db(&self) -> f32 {
        self.0 as f32 / 100.0
    }

    pub fn to_cdb(&self) -> i16 {
        self.0
    }
}

impl Display for SNR {
    fn fmt(&self, f: &mut core::fmt::Formatter<'_>) -> core::fmt::Result {
        write!(f, "{:.2} dB", self.to_db())
    }
}

use core::ops::{Deref, DerefMut};

use super::{Deserialize, Serialize};

#[derive(Debug, Clone, Copy)]
pub struct Chunk<const N: usize> {
    len: u8,
    buf: [u8; N],
}

impl<const N: usize> Chunk<N> {
    pub const CHUNK_SIZE_MAX: usize = N;

    pub fn new(len: usize) -> Self {
        assert!(len <= N, "Chuck must be <= {}", N);
        Self {
            len: len as u8,
            buf: [0_u8; N],
        }
    }

    #[inline]
    pub fn len(&self) -> usize {
        self.len as usize
    }

    pub fn try_from<B>(v: B) -> Result<Self, crate::Error>
    where
        B: AsRef<[u8]>,
    {
        let buf = v.as_ref();
        if buf.len() > Self::CHUNK_SIZE_MAX {
            return Err(crate::Error::TooBig);
        }

        let len = buf.len().min(Self::CHUNK_SIZE_MAX);

        let mut r = Self {
            len: len as u8,
            buf: [0_u8; N],
        };

        r.buf[..len].copy_from_slice(buf);
        Ok(r)
    }
}

impl<const N: usize> Deref for Chunk<N> {
    type Target = [u8];

    fn deref(&self) -> &Self::Target {
        &self.buf[..self.len as usize]
    }
}

impl<const N: usize> DerefMut for Chunk<N> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.buf[..self.len as usize]
    }
}

impl<const N: usize> Deserialize for Chunk<N> {
    fn deserialize(mut c: &mut crate::io::Cursor<&[u8]>) -> Result<Self, crate::prelude::Error> {
        Ok(Chunk {
            len: u8::deserialize(&mut c)?,
            buf: <[u8; N]>::deserialize(&mut c)?,
        })
    }
}

impl<const N: usize> Serialize for Chunk<N> {
    fn serialize(
        &self,
        mut buf: &mut crate::io::Cursor<&mut [u8]>,
    ) -> Result<(), crate::prelude::Error> {
        self.len.serialize(&mut buf)?;
        self.buf.serialize(&mut buf)?;
        Ok(())
    }
}

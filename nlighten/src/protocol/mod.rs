mod chunk;

use crate::io::{Cursor, Read, ReadExt, WriteExt};
use crate::Error;
pub use chunk::Chunk;

pub use super::frame::rtp;

pub const PAYLOAD_SIZE: usize = 128;
pub const FRAME_SIZE: usize = PAYLOAD_SIZE + 3 + 1;

pub type SpiFrame = super::frame::Frame<FRAME_SIZE>;

// msgchunks have 1 byte for the chunk size
pub const MSG_CHUNK_SIZE: usize = PAYLOAD_SIZE - 1;
pub type MsgChunk = Chunk<MSG_CHUNK_SIZE>;

// stream chunks have 1 byte for the stream num and 1 byte for the chunk size
pub const STREAM_CHUNK_SIZE: usize = PAYLOAD_SIZE - 2;
pub type MsgStreamChunk = Chunk<STREAM_CHUNK_SIZE>;

impl SpiFrame {
    #[inline]
    pub fn header_0(&self) -> Result<rtp::Header, rtp::Error> {
        rtp::Header::try_from(self.payload()[0])
    }

    pub fn header_0_set(&mut self, hdr: rtp::Header) {
        self.payload_mut()[0] = hdr.into();
    }

    #[inline]
    pub fn header_1(&self) -> Result<rtp::Header, rtp::Error> {
        rtp::Header::try_from(self.payload()[1])
    }

    pub fn header_1_set(&mut self, hdr: rtp::Header) {
        self.payload_mut()[1] = hdr.into();
    }

    #[inline]
    pub fn msg_type(&self) -> u8 {
        self.payload()[2]
    }

    #[inline]
    fn msg_payload(&self) -> &[u8] {
        &self.payload()[3..]
    }

    #[inline]
    fn msg_payload_mut(&mut self) -> &mut [u8] {
        &mut self.payload_mut()[3..]
    }

    pub fn msg(&self) -> Result<Option<Msg>, Error> {
        let msg_type = self.msg_type();
        let mut msg_payload = Cursor::new(self.msg_payload());
        match msg_type {
            0 => Ok(None),
            1 => Ok(Some(Msg::SX130xRegWrite(SX130xRegValue::deserialize(
                &mut msg_payload,
            )?))),
            2 => Ok(Some(Msg::SX130xRegRead(SX130xAddress::deserialize(
                &mut msg_payload,
            )?))),
            3 => Ok(Some(Msg::SX130xRegRMW(SX130xRegRMW::deserialize(
                &mut msg_payload,
            )?))),
            4 => Ok(Some(Msg::SX130xRegValue(SX130xRegValue::deserialize(
                &mut msg_payload,
            )?))),
            5 => Ok(Some(Msg::SX130xMemWrite(SX130xMemWrite::deserialize(
                &mut msg_payload,
            )?))),
            6 => Ok(Some(Msg::SX130xMemRead(SX130xMemRead::deserialize(
                &mut msg_payload,
            )?))),
            7 => Ok(Some(Msg::SX1250Read(SX1250Read::deserialize(
                &mut msg_payload,
            )?))),
            8 => Ok(Some(Msg::SX1250Write(SX1250Write::deserialize(
                &mut msg_payload,
            )?))),
            9 => Ok(Some(Msg::SMCUWrite(SMCURegValue::deserialize(
                &mut msg_payload,
            )?))),
            10 => Ok(Some(Msg::SMCURegRead(SMCUAddress::deserialize(
                &mut msg_payload,
            )?))),
            11 => Ok(Some(Msg::SMCURegRMW(SMCURegRMW::deserialize(
                &mut msg_payload,
            )?))),
            12 => Ok(Some(Msg::SMCURegValue(SMCURegValue::deserialize(
                &mut msg_payload,
            )?))),
            13 => Ok(Some(Msg::Data(MsgChunk::deserialize(&mut msg_payload)?))),
            14 => Ok(Some(Msg::StreamData(
                u8::deserialize(&mut msg_payload)?,
                MsgStreamChunk::deserialize(&mut msg_payload)?,
            ))),
            15 => Ok(Some(Msg::Shutdown)),
            16 => Ok(Some(Msg::SX1250Data(
                SX1250Read::deserialize(&mut msg_payload)?,
                <[u8; 5]>::deserialize(&mut msg_payload)?,
            ))),
            _ => Err(Error::Unknown),
        }
    }

    pub fn set_msg(&mut self, msg: Msg) -> Result<(), Error> {
        let mut msg_payload = Cursor::new(self.msg_payload_mut());
        let msg_type = match msg {
            Msg::SX130xRegWrite(d) => {
                d.serialize(&mut msg_payload)?;
                1
            }
            Msg::SX130xRegRead(d) => {
                d.serialize(&mut msg_payload)?;
                2
            }
            Msg::SX130xRegRMW(d) => {
                d.serialize(&mut msg_payload)?;
                3
            }
            Msg::SX130xRegValue(d) => {
                d.serialize(&mut msg_payload)?;
                4
            }
            Msg::SX130xMemWrite(d) => {
                d.serialize(&mut msg_payload)?;
                5
            }
            Msg::SX130xMemRead(d) => {
                d.serialize(&mut msg_payload)?;
                6
            }
            Msg::SX1250Read(d) => {
                d.serialize(&mut msg_payload)?;
                7
            }
            Msg::SX1250Write(d) => {
                d.serialize(&mut msg_payload)?;
                8
            }
            Msg::SMCUWrite(d) => {
                d.serialize(&mut msg_payload)?;
                9
            }
            Msg::SMCURegRead(d) => {
                d.serialize(&mut msg_payload)?;
                10
            }
            Msg::SMCURegRMW(d) => {
                d.serialize(&mut msg_payload)?;
                11
            }
            Msg::SMCURegValue(d) => {
                d.serialize(&mut msg_payload)?;
                12
            }
            Msg::Data(d) => {
                d.serialize(&mut msg_payload)?;
                13
            }
            Msg::StreamData(idx, chunk) => {
                idx.serialize(&mut msg_payload)?;
                chunk.serialize(&mut msg_payload)?;
                14
            }
            Msg::Shutdown => 15,
            Msg::SX1250Data(r, b) => {
                r.serialize(&mut msg_payload)?;
                b.serialize(&mut msg_payload)?;
                16
            }
        };
        self.payload_mut()[2] = msg_type;
        Ok(())
    }
}

#[derive(Debug, Clone, Copy)]
pub enum Msg {
    SX130xRegWrite(SX130xRegValue),
    SX130xRegRead(SX130xAddress),
    SX130xRegRMW(SX130xRegRMW),
    SX130xRegValue(SX130xRegValue),
    SX130xMemWrite(SX130xMemWrite),
    SX130xMemRead(SX130xMemRead),

    SX1250Read(SX1250Read),
    SX1250Write(SX1250Write),

    SMCUWrite(SMCURegValue),
    SMCURegRead(SMCUAddress),
    SMCURegRMW(SMCURegRMW),
    SMCURegValue(SMCURegValue),

    Data(MsgChunk),
    StreamData(u8, MsgStreamChunk),
    SX1250Data(SX1250Read, [u8; 5]),
    Shutdown,
}

trait Deserialize: Sized {
    fn deserialize(c: &mut Cursor<&[u8]>) -> Result<Self, Error>;
}

trait Serialize {
    fn serialize(&self, buf: &mut Cursor<&mut [u8]>) -> Result<(), Error>;
}

#[derive(Debug, Clone, Copy)]
#[cfg_attr(feature = "defmt", derive(::defmt::Format))]
pub struct RegValue<A, V> {
    pub address: A,
    pub value: V,
}

impl<A, V> Deserialize for RegValue<A, V>
where
    A: Deserialize,
    V: Deserialize,
{
    fn deserialize(mut c: &mut Cursor<&[u8]>) -> Result<Self, Error> {
        Ok(RegValue {
            address: A::deserialize(&mut c)?,
            value: V::deserialize(&mut c)?,
        })
    }
}

impl<A, V> Serialize for RegValue<A, V>
where
    A: Serialize,
    V: Serialize,
{
    fn serialize(&self, mut buf: &mut Cursor<&mut [u8]>) -> Result<(), Error> {
        self.address.serialize(&mut buf)?;
        self.value.serialize(&mut buf)?;
        Ok(())
    }
}

#[derive(Debug, Clone, Copy)]
#[cfg_attr(feature = "defmt", derive(::defmt::Format))]
pub struct RegRMW<A, V> {
    pub address: A,
    pub value: V,
    pub mask: V,
}

impl<A, V> Deserialize for RegRMW<A, V>
where
    A: Deserialize,
    V: Deserialize,
{
    fn deserialize(mut c: &mut Cursor<&[u8]>) -> Result<Self, Error> {
        Ok(RegRMW {
            address: A::deserialize(&mut c)?,
            value: V::deserialize(&mut c)?,
            mask: V::deserialize(&mut c)?,
        })
    }
}

impl<A, V> Serialize for RegRMW<A, V>
where
    A: Serialize,
    V: Serialize,
{
    fn serialize(&self, mut buf: &mut Cursor<&mut [u8]>) -> Result<(), Error> {
        self.address.serialize(&mut buf)?;
        self.value.serialize(&mut buf)?;
        self.mask.serialize(&mut buf)?;
        Ok(())
    }
}

#[derive(Debug, Clone, Copy)]
#[cfg_attr(feature = "defmt", derive(::defmt::Format))]
pub struct MemWrite<A, L> {
    pub address: A,
    pub len: L,
}

impl<A, L> Deserialize for MemWrite<A, L>
where
    A: Deserialize,
    L: Deserialize,
{
    fn deserialize(mut c: &mut Cursor<&[u8]>) -> Result<Self, Error> {
        Ok(MemWrite {
            address: A::deserialize(&mut c)?,
            len: L::deserialize(&mut c)?,
        })
    }
}

impl<A, L> Serialize for MemWrite<A, L>
where
    A: Serialize,
    L: Serialize,
{
    fn serialize(&self, mut buf: &mut Cursor<&mut [u8]>) -> Result<(), Error> {
        self.address.serialize(&mut buf)?;
        self.len.serialize(&mut buf)?;
        Ok(())
    }
}

#[derive(Debug, Clone, Copy)]
#[cfg_attr(feature = "defmt", derive(::defmt::Format))]
pub struct MemRead<A, L> {
    pub address: A,
    pub len: L,
    pub fifo_mode: bool,
}

impl<A, L> Deserialize for MemRead<A, L>
where
    A: Deserialize,
    L: Deserialize,
{
    fn deserialize(mut c: &mut Cursor<&[u8]>) -> Result<Self, Error> {
        Ok(MemRead {
            address: A::deserialize(&mut c)?,
            len: L::deserialize(&mut c)?,
            fifo_mode: bool::deserialize(&mut c)?,
        })
    }
}

impl<A, L> Serialize for MemRead<A, L>
where
    A: Serialize,
    L: Serialize,
{
    fn serialize(&self, mut buf: &mut Cursor<&mut [u8]>) -> Result<(), Error> {
        self.address.serialize(&mut buf)?;
        self.len.serialize(&mut buf)?;
        self.fifo_mode.serialize(&mut buf)?;
        Ok(())
    }
}

pub type SX130xAddress = u16;
pub type SX130xRegValue = RegValue<SX130xAddress, u8>;
pub type SX130xRegRMW = RegRMW<SX130xAddress, u8>;
pub type SX130xMemWrite = MemWrite<SX130xAddress, u16>;
pub type SX130xMemRead = MemRead<SX130xAddress, u16>;

pub type SMCUAddress = u16;
pub type SMCURegValue = RegValue<SMCUAddress, u32>;
pub type SMCURegRMW = RegRMW<SMCUAddress, u32>;

#[derive(Debug, Clone, Copy)]
#[cfg_attr(feature = "defmt", derive(::defmt::Format))]
pub struct SX1250Read {
    pub radio: u8,
    pub opcode: u8,
    pub buf_size: u8,
}

impl Deserialize for SX1250Read {
    fn deserialize(mut c: &mut Cursor<&[u8]>) -> Result<Self, Error> {
        Ok(SX1250Read {
            radio: u8::deserialize(&mut c)?,
            opcode: u8::deserialize(&mut c)?,
            buf_size: u8::deserialize(&mut c)?,
        })
    }
}

impl Serialize for SX1250Read {
    fn serialize(&self, mut buf: &mut Cursor<&mut [u8]>) -> Result<(), Error> {
        self.radio.serialize(&mut buf)?;
        self.opcode.serialize(&mut buf)?;
        self.buf_size.serialize(&mut buf)?;
        Ok(())
    }
}

#[derive(Debug, Clone, Copy)]
#[cfg_attr(feature = "defmt", derive(::defmt::Format))]
pub struct SX1250Write {
    pub radio: u8,
    pub opcode: u8,
    pub buf_size: u8,
    pub buf: [u8; 5],
}

impl Deserialize for SX1250Write {
    fn deserialize(mut c: &mut Cursor<&[u8]>) -> Result<Self, Error> {
        Ok(SX1250Write {
            radio: u8::deserialize(&mut c)?,
            opcode: u8::deserialize(&mut c)?,
            buf_size: u8::deserialize(&mut c)?,
            buf: <[u8; 5]>::deserialize(&mut c)?,
        })
    }
}

impl Serialize for SX1250Write {
    fn serialize(&self, mut buf: &mut Cursor<&mut [u8]>) -> Result<(), Error> {
        self.radio.serialize(&mut buf)?;
        self.opcode.serialize(&mut buf)?;
        self.buf_size.serialize(&mut buf)?;
        self.buf.serialize(&mut buf)?;
        Ok(())
    }
}

impl Deserialize for u8 {
    fn deserialize(c: &mut Cursor<&[u8]>) -> Result<Self, Error> {
        c.read_u8()
    }
}

impl Serialize for u8 {
    fn serialize(&self, buf: &mut Cursor<&mut [u8]>) -> Result<(), Error> {
        buf.write_u8(*self);
        Ok(())
    }
}

impl Deserialize for u16 {
    fn deserialize(c: &mut Cursor<&[u8]>) -> Result<Self, Error> {
        c.read_u16()
    }
}

impl Serialize for u16 {
    fn serialize(&self, buf: &mut Cursor<&mut [u8]>) -> Result<(), Error> {
        buf.write_u16(*self);
        Ok(())
    }
}

impl Deserialize for u32 {
    fn deserialize(c: &mut Cursor<&[u8]>) -> Result<Self, Error> {
        c.read_u32()
    }
}

impl Serialize for u32 {
    fn serialize(&self, buf: &mut Cursor<&mut [u8]>) -> Result<(), Error> {
        buf.write_u32(*self);
        Ok(())
    }
}

impl Deserialize for bool {
    fn deserialize(c: &mut Cursor<&[u8]>) -> Result<Self, Error> {
        let v = c.read_u8()?;
        Ok(v > 0)
    }
}

impl Serialize for bool {
    fn serialize(&self, mut buf: &mut Cursor<&mut [u8]>) -> Result<(), Error> {
        let v = match self {
            true => 0xff_u8,
            false => 0x00_u8,
        };
        v.serialize(&mut buf)
    }
}

impl<const N: usize> Deserialize for [u8; N] {
    fn deserialize(c: &mut Cursor<&[u8]>) -> Result<Self, Error> {
        let mut v = [0_u8; N];
        c.read_exact(&mut v)?;
        Ok(v)
    }
}

impl<const N: usize> Serialize for [u8; N] {
    fn serialize(&self, buf: &mut Cursor<&mut [u8]>) -> Result<(), Error> {
        buf.write_slice(&self);
        Ok(())
    }
}

#[cfg(test)]
mod test {

    use super::*;

    #[test]
    fn test_deserialize_uninit_spiframe() {
        let frame = SpiFrame::new();
        let m = frame.msg().unwrap();
        assert!(m.is_none());
    }

    #[test]
    fn test_spi_frame_payload_size() {
        let frame = SpiFrame::new();
        let msg_payload = frame.msg_payload();
        assert_eq!(PAYLOAD_SIZE, msg_payload.len());
    }

    #[test]
    fn test_spi_frame_stream() -> Result<(), crate::Error> {
        let stream_chunk = MsgStreamChunk::try_from(b"hello world")?;
        let msg = Msg::StreamData(3, stream_chunk);

        let mut frame = SpiFrame::new();
        frame.set_msg(msg)?;

        let msg_out = frame.msg().unwrap().unwrap();
        match msg_out {
            Msg::StreamData(idx, ref chunk) => {
                assert_eq!(3, idx);
                assert_eq!(b"hello world".as_slice(), chunk.as_ref());
            }

            _ => panic!("expecting StreamData"),
        };

        Ok(())
    }
}

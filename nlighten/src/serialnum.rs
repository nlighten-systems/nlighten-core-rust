// Copyright (C) NLighten Systems, LLC (Paul Soucy) 2022
//
// This file is part of Kompressor Firmware.
//
// Kompressor Firmware is free software: you can redistribute it and/or modify it under the terms of
// the GNU General Public License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any later version.
//
// Kompressor Firmware is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with Foobar. If not, see
// <https://www.gnu.org/licenses/>.

use core::{fmt::Display, str::FromStr};

use chrono::Datelike;
use safe_regex::Matcher5;
use serde::{Deserialize, Serialize};

#[derive(Debug, PartialEq, Eq)]
pub enum Error {
    InvalidDate,
    InvalidModel,
    InvalidSerial,
}

impl Display for Error {
    fn fmt(&self, f: &mut core::fmt::Formatter<'_>) -> core::fmt::Result {
        write!(f, "{}", self)
    }
}

/// Secure Concentrator Serial Number
/// 29 Model (3-bits)
/// 28 RMA (1-bit) = 0 original, 1 = refurbished
/// 25 Reserved (3-bits)
/// 20 Year(5-bits) 0 = 2022
/// 14 week(6-bits) week of the year (values >= 52 are invalid)
/// 0  Serial (14-bits)
#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Serialize, Deserialize)]
pub struct SerialNum(u32);

impl SerialNum {
    pub fn try_new<D: Datelike>(date: D, serial: u32, rma: bool, model: u8) -> Result<Self, Error> {
        let (is_ce, year) = date.year_ce();
        if !is_ce {
            return Err(Error::InvalidDate);
        }
        let week = date.iso_week().week0();
        Self::try_from(year, week, serial, rma, model)
    }

    pub fn try_from(
        year: u32,
        week: u32,
        serial: u32,
        rma: bool,
        model: u8,
    ) -> Result<Self, Error> {
        if year < 2022 || year > 2053 {
            return Err(Error::InvalidDate);
        }
        if week > 52 {
            return Err(Error::InvalidDate);
        }
        if model > 7 {
            return Err(Error::InvalidModel);
        }
        if serial > 0x0000_3FFF {
            return Err(Error::InvalidSerial);
        }
        let year = year - 2022;

        let serialnum = ((model as u32) << 29)
            | if rma { 1 << 28 } else { 0 }
            | year << 20
            | week << 14
            | serial;

        Ok(Self(serialnum))
    }

    pub const fn from_bytes(bytes: [u8; 4]) -> Self {
        Self(u32::from_be_bytes(bytes))
    }

    pub fn to_bytes(&self) -> [u8; 4] {
        self.0.to_be_bytes()
    }

    pub fn model(&self) -> u8 {
        (0x0000_0007 & (self.0 >> 29)) as u8
    }

    pub fn rma(&self) -> bool {
        (0x1000_0000 & self.0) > 0
    }

    pub fn year(&self) -> u8 {
        (0x0000_001F & (self.0 >> 20)) as u8
    }

    pub fn week(&self) -> u8 {
        (0x0000_003F & (self.0 >> 14)) as u8
    }

    pub fn serial(&self) -> u32 {
        0x0000_3FFF & self.0
    }

    #[inline]
    pub fn raw(&self) -> u32 {
        self.0
    }
}

impl From<u32> for SerialNum {
    fn from(v: u32) -> Self {
        SerialNum(v)
    }
}

impl Display for SerialNum {
    fn fmt(&self, f: &mut core::fmt::Formatter<'_>) -> core::fmt::Result {
        let model = unsafe { char::from_u32_unchecked(65 + self.model() as u32) };
        let rma = if self.rma() { 1 } else { 0 };
        write!(
            f,
            "{}{:02x}{:02}{:01}{:05}",
            model,
            self.year(),
            self.week(),
            rma,
            self.serial()
        )
    }
}

impl FromStr for SerialNum {
    type Err = Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let matcher: Matcher5<_> =
            safe_regex::regex!(br"(.)([0-9a-fA-F]{2})([0-9]{2})([0-1]{1})([0-9]{5})");
        match matcher.match_slices(s.as_bytes()) {
            Some((model, year, week, rma, serial)) => {
                let model = model[0] - 65;
                let year = u32::from_str_radix(unsafe { core::str::from_utf8_unchecked(year) }, 16)
                    .map_err(|_| Error::InvalidSerial)?
                    + 2022;
                let week = u8::from_str_radix(unsafe { core::str::from_utf8_unchecked(week) }, 10)
                    .map_err(|_| Error::InvalidSerial)?;
                let rma = u8::from_str_radix(unsafe { core::str::from_utf8_unchecked(rma) }, 10)
                    .map_err(|_| Error::InvalidSerial)?;
                let rma = if rma > 0 { true } else { false };
                let serial =
                    u32::from_str_radix(unsafe { core::str::from_utf8_unchecked(serial) }, 10)
                        .map_err(|_| Error::InvalidSerial)?;

                Self::try_from(year, week.into(), serial, rma, model)
            }

            None => Err(Error::InvalidSerial),
        }
    }
}

#[cfg(test)]
mod test {

    use super::*;
    use chrono::NaiveDate;

    #[test]
    fn test_new() {
        let today = NaiveDate::from_ymd_opt(2022, 11, 30).unwrap();
        let serial = SerialNum::try_new(today, 1, false, 0);
        let serial = serial.unwrap();
        println!("serial: {}", serial);
        println!("serial: {}", serial.raw());
    }

    #[test]
    fn test_try_new() {
        let today = NaiveDate::from_ymd_opt(2023, 12, 15).unwrap();
        let serial = SerialNum::try_new(today, 52, true, 0);
        assert_eq!(Ok(SerialNum(270286900)), serial);
        assert_eq!("A0149100052", serial.unwrap().to_string());
    }

    #[test]
    fn test_from_str() {
        let v = SerialNum::from_str("A0149100052");
        assert_eq!(Ok(SerialNum(270286900)), v);

        let v = SerialNum::from_str("A0149000052").unwrap();
        let serial =
            SerialNum::try_new(NaiveDate::from_ymd_opt(2023, 12, 15).unwrap(), 52, false, 0)
                .unwrap();
        assert_eq!(v, serial);
    }
}

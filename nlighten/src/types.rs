use ed25519_compact::Signature;
use serde::{Deserialize, Serialize};

use crate::{
    gps::{GPSTime, WGS84Position},
    lora::{CodingRate, Datarate, Payload, RxPkt},
};

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct FullRxPkt {
    pub id: u32,
    pub pkt: RxPkt,
    /// RSSI of the channel
    pub rssic: u8,
    /// was the crc enabled
    pub crc_en: bool,
    /// was there a crc error
    pub crc_err: bool,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct RxPktSig {
    pub id: u32,
    #[serde(with = "signature")]
    pub sig: Signature,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct TxPkt {
    /// center frequency of TX
    pub freq_hz: u32,
    pub rf_chain: u8,
    /// TX power, in dBm
    pub rf_power: i8,

    pub datarate: Datarate,
    pub coderate: CodingRate,
    /// invert signal polarity, for orthogonal downlinks (LoRa only)
    pub invert_pol: bool,
    pub preamble: Option<u16>,
    pub no_crc: bool,
    pub no_header: bool,
    pub payload: Payload,
    pub tx_mode: TxMode,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub enum TxMode {
    Immediate,
    Timestamped(u32),
    OnGPS,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct TxReceipt {
    pub freq: u32,
    pub datarate: Datarate,
    pub gps_time: Option<GPSTime>,
    pub pos: Option<WGS84Position>,
    pub payload: heapless::Vec<u8, 256>,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct NonRFSig {
    #[serde(with = "signature")]
    pub sig: Signature,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub enum SCStreamMsg {
    RxPkt(FullRxPkt),
    RxPktSig(RxPktSig),
    TxReceipt(TxReceipt),
    NonRFSig(NonRFSig),
}

pub(crate) mod signature {
    use ed25519_compact::Signature;
    use serde::{de::Visitor, Deserializer, Serializer};

    pub fn serialize<S>(v: &Signature, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        serializer.serialize_bytes(v.as_ref())
    }

    pub fn deserialize<'de, D>(deserializer: D) -> Result<Signature, D::Error>
    where
        D: Deserializer<'de>,
    {
        struct MyVisitor;
        impl<'de> Visitor<'de> for MyVisitor {
            type Value = Signature;

            fn expecting(&self, formatter: &mut core::fmt::Formatter) -> core::fmt::Result {
                write!(formatter, "a byte array of size Signature::BYTES")
            }

            fn visit_seq<A>(self, mut seq: A) -> Result<Self::Value, A::Error>
            where
                A: serde::de::SeqAccess<'de>,
            {
                let mut bytes = Vec::with_capacity(Signature::BYTES);

                while let Some(b) = seq.next_element()? {
                    bytes.push(b);
                }

                if bytes.len() != Signature::BYTES {
                    return Err(serde::de::Error::invalid_length(bytes.len(), &self));
                }

                Ok(Signature::new(bytes.try_into().unwrap()))
            }

            fn visit_bytes<E>(self, v: &[u8]) -> Result<Self::Value, E>
            where
                E: serde::de::Error,
            {
                if v.len() != Signature::BYTES {
                    return Err(E::custom(format_args!("invalid len")));
                }

                let sig = Signature::new(v.try_into().unwrap());
                Ok(sig)
            }
        }

        deserializer.deserialize_bytes(MyVisitor)
    }
}

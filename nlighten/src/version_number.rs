#[cfg(feature = "std")]
use core::str::FromStr;
use core::{cmp::Ordering, fmt::Display};

/// Version number encodes major.minor.patch-<git-hash> with 32-bits
/// major - 3-bits
/// minor - 6-bits
/// patch - 7-bits
#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
pub struct VersionNumber(pub u32);

impl VersionNumber {
    pub const NO_ID: u32 = 0;

    pub const fn new(major: u32, minor: u32, patch: u32, id: Option<u16>) -> Self {
        let major = 0x03 & major;
        let minor = 0x3F & minor;
        let patch = 0x7F & patch;

        let mut v = major << 29 | minor << 23 | patch << 16;
        if let Some(id) = id {
            v |= id as u32;
        }
        Self(v)
    }

    pub fn major(&self) -> u32 {
        0x03 & self.0 >> 29
    }

    pub fn minor(&self) -> u32 {
        0x3F & self.0 >> 23
    }

    pub fn patch(&self) -> u32 {
        0x7F & self.0 >> 16
    }

    pub fn id(&self) -> u32 {
        0xFFFF & self.0
    }
}

impl Display for VersionNumber {
    fn fmt(&self, f: &mut core::fmt::Formatter<'_>) -> core::fmt::Result {
        let major = self.major();
        let minor = self.minor();
        let patch = self.patch();
        let id = self.id();

        if id == Self::NO_ID {
            write!(f, "{}.{}.{}", major, minor, patch)
        } else {
            write!(f, "{}.{}.{}-g{:04x}", major, minor, patch, id)
        }
    }
}

impl From<u32> for VersionNumber {
    fn from(value: u32) -> Self {
        VersionNumber(value)
    }
}

impl PartialOrd for VersionNumber {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        let r = self.major().cmp(&other.major());
        if Ordering::Equal != r {
            return Some(r);
        }

        let r = self.minor().cmp(&other.minor());
        if Ordering::Equal != r {
            return Some(r);
        }

        let r = self.patch().cmp(&other.patch());
        Some(r)
    }
}

impl Ord for VersionNumber {
    fn cmp(&self, other: &Self) -> core::cmp::Ordering {
        self.partial_cmp(other).unwrap()
    }
}

#[cfg(feature = "std")]
impl FromStr for VersionNumber {
    type Err = crate::Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        use regex::Regex;

        let regex = Regex::new(r"(\d{1})\.(\d{1,2})\.(\d{1,3})(.*g([0-9a-fA-F]{4}))?").unwrap();
        match regex.captures(s) {
            Some(groups) => {
                let major = u32::from_str(&groups[1]).unwrap();
                let minor = u32::from_str(&groups[2]).unwrap();
                let patch = u32::from_str(&groups[3]).unwrap();

                let id = match groups.get(5) {
                    Some(m) => Some(u16::from_str_radix(m.as_str(), 16).unwrap()),
                    None => None,
                };

                Ok(VersionNumber::new(major, minor, patch, id))
            }
            None => Err(crate::Error::NotFound),
        }
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_ordering() {
        assert!(VersionNumber::new(1, 6, 0, None) > VersionNumber::new(1, 5, 345, None));
    }

    #[test]
    fn test_parse() {
        let v = VersionNumber::from_str("1.3.4");
        assert_eq!(Ok(VersionNumber::new(1, 3, 4, None)), v);

        let v = VersionNumber::from_str("0.1.0-15-ga007ea9");
        assert_eq!(Ok(VersionNumber::new(0, 1, 0, Some(0xa007))), v);
    }

    #[test]
    fn test_display() {
        let v = VersionNumber::new(1, 5, 6, None);
        assert_eq!("1.5.6", v.to_string());

        let v = VersionNumber::new(1, 4, 8, Some(0x7abc));
        assert_eq!("1.4.8-g7abc", v.to_string());
    }
}
